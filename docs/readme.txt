## If shop admin does not want to show the payment methods' name during the checkout, please follow the instruction below:
--------------------------------------------------------------------------------------------------------------------------
1. go to file {your shop root folder}/wp-content/plugins/{woocommerce plugin}/template/checkout/payment-method.php

at line 18
		<?php echo $gateway->get_title(); ?> <?php echo $gateway->get_icon(); ?>

change to this code :
		<?php
			if (strpos($gateway->id, 'zionpayment') === false){
                echo $gateway->get_title(); 
            }
		?> 
		<?php echo $gateway->get_icon(); ?>


## If shop admin does not want to show the payment methods' name during the order detail, please follow the instruction below:
------------------------------------------------------------------------------------------------------------------------------
1. go to file {your shop root folder}/wp-content/plugins/{woocommerce plugin}/template/order/order-details.php

at line 39
			foreach ( $order->get_order_item_totals() as $key => $total ) {
				?>
				<tr>
					<th scope="row"><?php echo $total['label']; ?></th>
					<td><?php echo $total['value']; ?></td>
				</tr>
				<?php
			}

change to this code :

			global $wpdb;

			$isZionPayment = false;
	        $query = $wpdb->prepare("SELECT meta_value FROM {$wpdb->prefix}postmeta WHERE meta_key = '_payment_method' AND post_id = %d", $order->id);
	        $row = $wpdb->get_row($query, ARRAY_A);
			$payment_method = $row['meta_value'];
			if (strpos($payment_method, 'zionpayment') !== false)
				$isZionPayment = true;

			foreach ( $order->get_order_item_totals() as $key => $total ) {
				if (!$isZionPayment || ($isZionPayment && $total['label'] != 'Payment Method:')):
				?>
				<tr>
					<th scope="row"><?php echo $total['label']; ?></th>
					<td><?php echo $total['value']; ?></td>
				</tr>
				<?php
				endif;
			}

## If shop admin does not want to show the payment methods' name during the checkout form, please follow the instruction below:
-------------------------------------------------------------------------------------------------------------------------------
1. go to file {your shop root folder}/wp-content/plugins/{woocommerce plugin}/include/shortcodes/class-wc-shortcode-checkout.php

at line 139

						<?php if ($order->payment_method_title) : ?>
						<li class="method">
							<?php _e( 'Payment Method:', 'woocommerce' ); ?>
							<strong><?php
								echo $order->payment_method_title;
							?></strong>
						</li>
						<?php endif; ?>


change to this code :

						<?php
						if ($order->payment_method_title) :

							global $wpdb;
							$order_id = $order->get_order_number();

					        $query = $wpdb->prepare("SELECT meta_value FROM {$wpdb->prefix}postmeta WHERE meta_key = '_payment_method' AND post_id = %d", $order_id);
					        $row = $wpdb->get_row($query, ARRAY_A);
							$payment_method = $row['meta_value'];

							if (strpos($payment_method, 'zionpayment') === false):
								?>
								<li class="method">
									<?php _e( 'Payment Method:', 'woocommerce' ); ?>
										<strong><?php
											echo $order->payment_method_title;
										?></strong>
								</li>
								<?php
							endif;
						endif;
						?>

## If shop admin does not want to show the payment methods' name during the success order, please follow the instruction below:
-------------------------------------------------------------------------------------------------------------------------------
1. go to file {your shop root folder}/wp-content/plugins/{woocommerce plugin}/template/checkout/thankyou.php

at line 51

			<?php if ( $order->payment_method_title ) : ?>
			<li class="method">
				<?php _e( 'Payment Method:', 'woocommerce' ); ?>
				<strong><?php echo $order->payment_method_title; ?></strong>
			</li>
			<?php endif; ?>

change to this code :

			<?php
			if ( $order->payment_method_title ) :

				global $wpdb;
				$order_id = $order->get_order_number();
		        $query = $wpdb->prepare("SELECT meta_value FROM {$wpdb->prefix}postmeta WHERE meta_key = '_payment_method' AND post_id = %d", $order_id);
		        $row = $wpdb->get_row($query, ARRAY_A);
				$payment_method = $row['meta_value'];

				if (strpos($payment_method, 'zionpayment') === false):
					?>
					<li class="method">
						<?php _e( 'Payment Method:', 'woocommerce' ); ?>
						<strong><?php echo $order->payment_method_title; ?></strong>
					</li>
					<?php
				endif;
			endif;
			?>

***********************************************************************************************************************	
***********************************************************************************************************************		

## Add My Account, Shop, Cart and Chackout Menu at homepage

1. login to backend
2. Click Apperance -> Menus
3. Create New Menu at right panel and add My Account My Account, Shop, Cart and Chackout from left panel

## Zionpayment General Setting

1. login to backend
2. Click Woocommerce -> Settings
3. Click Tab : Zionpayment General Setting

## Zionpayment Payment Setting

1. login to backend
2. Click Woocommerce -> Settings
3. Click Tab : Checkout
4. Choose the payment on top list

## Remove index.php at URL structure (APACHE)

1. create .htacess file
2. add this code at root:

A. if shop location on main folder ex: (http://my-domain.com/)
# BEGIN WordPress
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
</IfModule>
# END WordPress

B. if shop location on sub folder ex: (http://my-domain.com/my-shop/)

# BEGIN WordPress
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /my-shop/
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /my-shop/index.php [L]
</IfModule>
# END WordPress

## Remove index.php at URL structure (NGINX)

1 add this code at nginx configuration -> default

A. if shop location on main folder ex: (http://my-domain.com/)
location / {
    try_files $uri $uri/ /index.php?$args;
}


B. if shop location on sub folder ex: (http://my-domain.com/my-shop/)
location /my-shop/ {
    try_files $uri $uri/ /my-shop/index.php?$args;
}