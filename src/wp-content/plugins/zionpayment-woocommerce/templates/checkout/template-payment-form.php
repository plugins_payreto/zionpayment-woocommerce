<?php
/**
 * Sibs Payments Form
 *
 * The file is for displaying the Sibs payment form
 * Copyright (c) SIBS
 *
 * @package     Sibs/Templates
 * @located at  /template/ckeckout/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<script type="text/javascript">
	var wpwlOptions = {
		locale: "<?php echo esc_attr( strtolower( substr( get_bloginfo( 'language' ), 0, 2 ) ) ) ?>",
		style: "plain",
		onReady: function() {
			var zionpaymentLogo = '<div class="zionpayment_logo"><img src="<?php echo esc_attr( $this->plugins_url ) ?>/assets/images/zionpayment_logo.png" alt="ZIONPAYMENT" style="height:70px;" /></div>';
			var buttonCancel = "<a href='<?php echo esc_attr( $url_config['cancel_url'] ) ?>' class='wpwl-button btn_cancel'><?php echo esc_attr( __( 'FRONTEND_BT_CANCEL', 'wc-zionpayment' ) ) ?></a>";
			var ttTestMode = "<div class='testmode'><?php echo esc_attr( __( 'FRONTEND_TT_TESTMODE', 'wc-zionpayment' ) ) ?></div>";
			jQuery( "form.wpwl-form" ).find( ".wpwl-button" ).before( buttonCancel );
			jQuery( "form.wpwl-form" ).prepend(zionpaymentLogo);
			<?php if ( 'TEST' === $this->settings['server_mode'] ) : ?>
				jQuery( ".wpwl-container" ).wrap( "<div class='frametest'></div>" );
				jQuery( ".wpwl-container" ).before( ttTestMode );
			<?php endif; ?>
			<?php if ( $is_recurring && $is_one_click_payments ) : ?>
				var headerWidget = "<h3 id='deliveryHeader' style='text-align:center'><?php echo esc_attr( __( 'FRONTEND_RECURRING_WIDGET_HEADER2', 'wc-zionpayment' ) ) ?></h3>";
				jQuery( "#wpwl-registrations" ).after( headerWidget );
			<?php endif; ?>
		},
		registrations: {
			hideInitialPaymentForms: false,
			requireCvv: false
		}
	}
</script>
<?php if ( $is_recurring ) : ?>
	<?php if ( $is_one_click_payments ) : ?>
		<h3 id="deliveryHeader" style="text-align:center"><?php echo esc_attr( __( 'FRONTEND_RECURRING_WIDGET_HEADER1', 'wc-zionpayment' ) ) ?></h3>
	<?php else : ?>
		<h3 id="deliveryHeader" style="text-align:center"><?php echo esc_attr( __( 'FRONTEND_MC_PAYANDSAFE', 'wc-zionpayment' ) ) ?></h3>
	<?php endif; ?>
<?php endif; ?>

<form action="<?php echo esc_attr( $url_config['return_url'] ) ?>" class="paymentWidgets"><?php echo esc_attr( $payment_parameters['payment_brand'] ) ?></form>
