<?php
/**
 * Zionpayment Payments Update Order
 *
 * The file is for displaying additional information at order detail (admin)
 * Copyright (c) Zionpayment
 *
 * @package     Zionpayment/Templates
 * @located at  /template/admin/meta-boxes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<p>
	<strong><?php echo esc_attr( __( 'BACKEND_GENERAL_INFORMATION', 'wc-zionpayment' ) ); ?></strong>
</p>
<p>

</p>
<p>
	<?php
		echo esc_attr( __( 'BACKEND_TT_PAYMENT_METHOD', 'wc-zionpayment' ) ) . ' : ' . esc_attr( Zionpayment_General_Functions::zn_translate_backend_payment( $transaction_log['payment_id'] ) ) . '<br />';
		echo esc_attr( __( 'BACKEND_TT_CURRENCY', 'wc-zionpayment' ) ) . ' : ' . esc_attr( $transaction_log['currency'] ) . '<br />';
		echo esc_attr( __( 'BACKEND_TT_AMOUNT', 'wc-zionpayment' ) ) . ' : ' . esc_attr( $transaction_log['amount'] ) . '<br />';
		echo esc_attr( __( 'BACKEND_TT_TRANSACTION_ID', 'wc-zionpayment' ) ) . ' : ' . esc_attr( $transaction_log['transaction_id'] ) . '<br />';
	?>
</p>
