<?php
/**
 * Zionpayment Payments Form
 *
 * The file is for displaying the Zionpayment register form
 * Copyright (c) Zionpayment
 *
 * @package     Zionpayment/Templates
 * @located at  /template/ckeckout/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

	<h2 class="header-title header-center"><?php echo esc_attr( $form_title ) ?></h2>
	<script type="text/javascript">
		var wpwlOptions = {
				locale: "<?php echo esc_attr( strtolower( substr( get_bloginfo( 'language' ), 0, 2 ) ) ) ?>",
				 style: "plain",
				 onReady: function(){
					var buttonCancel = "<a href='<?php echo esc_attr( $url_config['cancel_url'] ) ?>' class='wpwl-button btn_cancel'><?php echo esc_attr( __( 'FRONTEND_BT_CANCEL', 'wc-zionpayment' ) ) ?></a>";
					var buttonConfirm = "<?php echo esc_attr( $confirm_button ) ?></a>";
					var ttTestMode = "<div class='testmode'><?php echo esc_attr( __( 'FRONTEND_TT_TESTMODE', 'wc-zionpayment' ) ) ?></div>";
					var ttRegistration = "<div class='register-tooltip'><?php echo esc_attr( __( 'FRONTEND_TT_REGISTRATION', 'wc-zionpayment' ) ) ?></div>";
		            jQuery("form.wpwl-form").find(".wpwl-button").before(buttonCancel);
		            jQuery(".wpwl-button-pay").html(buttonConfirm);
		            jQuery(".wpwl-container").after(ttRegistration);
		            <?php if ( 'TEST' === $register_parameters['server_mode'] ) : ?>
				        jQuery(".wpwl-container").wrap( "<div class='frametest'></div>");
				        jQuery('.wpwl-container').before(ttTestMode);
		            <?php endif; ?>
					jQuery("input[name=registrationId]").after("<label for='wpwl-wrapper-registration-registrationId'></label>");
					jQuery('.wpwl-container').css('border','1px solid #bbb').css('padding','25px');

					var ttCVVHelp = "<div class='help'><span class='txtHelp'><?php echo esc_attr( __( 'FRONTEND_CC_HELP','wc-zionpayment' ) ); ?></span></div>";
					var mastercardSecure = "<div class=\"btn_center_wrapper\"><a href=\"https://www.mastercard.us/en-us/consumers/features-benefits/securecode.html\" target=\"_blank\" class=\"secure_image_wrapper\"><img src=\"<?php echo esc_attr( self::$plugin_url ) ?>/assets/images/mastercard-secure.jpg\" alt=\"secureCode\" class=\"secure_image\" /></a></div>";
					var verifiedVisa = "<a href=\"https://www.visaeurope.com/making-payments/verified-by-visa/\" target=\"_blank\" class=\"verified_image_wrapper\"><img src=\"<?php echo esc_attr( self::$plugin_url ) ?>/assets/images/verified-visa.jpg\" alt=\"verified\" class=\"verified_image\" /></a>";
					var zionPaymentLogo = "<div class=\"zionPayment_logo\"><img src=\"<?php echo esc_attr( self::$plugin_url ) ?>/assets/images/logo_bs.png\" alt=\"zionpayment\" class=\"zionpayment_image\" /></div>";

					jQuery('.wpwl-container').prepend(zionPaymentLogo);
					jQuery('.wpwl-container-card').find('.zionPayment_logo').css('float','left');
					jQuery(".wpwl-control-cvv").closest(".wpwl-wrapper-cvv").append(ttCVVHelp);
					jQuery('form.wpwl-form').find('.wpwl-group-submit').after(mastercardSecure);
					jQuery('form.wpwl-form').find('.secure_image_wrapper').before(verifiedVisa);
					jQuery('.wpwl-brand-card').css('display','none');
					jQuery('.wpwl-group-cardHolder').after('<div style="clear:both;"></div>');

				<?php
						$brands = explode( ' ', $payment_brand );
						krsort( $brands );

				foreach ( $brands as $brand ) {
					$lower_brand = strtolower( $brand );
					if ( '' !== $brand ) {
					?>
				jQuery(".wpwl-brand:last").after( jQuery("<div style='opacity: 1;' class='wpwl-brand-card wpwl-brand-custom wpwl-brand-<?php echo esc_attr( $brand ) ?>'><img src='<?php echo esc_attr( self::$plugin_url ) ?>/assets/images/<?php echo esc_attr( $lower_brand ) ?>.png' alt='<?php echo esc_attr( $brand ) ?>' class='brand-image' /></div>"));
					<?php }
				} ?>
				getCVVHelp();
		} ,
			onChangeBrand: function(e){
				jQuery(".wpwl-brand-custom").css("opacity", "0.3");
				jQuery(".wpwl-brand-" + e).css("opacity", "1");
			},
			registrations: {
	             hideInitialPaymentForms: false,
	             requireCvv: true
	        },
			onReadyIframeCommunication: function(){
				var ttCVVHelp = "<div class='help'><span class='txtHelp'><?php echo esc_attr( __( 'FRONTEND_CC_HELP','wc-zionpayment' ) ); ?></span></div>";
				var helpLength = jQuery(".help").length;

				if (helpLength > 1) {
			        jQuery(".help").not(':last').remove();
			    } else {
					jQuery(".wpwl-selected").find(".wpwl-control-cvv").after(ttCVVHelp);
				}

				getCVVHelp();
			}
	};

	    function getCVVHelp() {
			jQuery(".help").click(function(e){
				e.preventDefault();
				jQuery(".overlay").fadeIn();
			});
			jQuery(".close-button").click(function(e){
				e.preventDefault();
			   jQuery(".overlay").fadeOut();
			});
		    }
	</script>

<form action="<?php echo esc_attr( $url_config['return_url'] ) ?>" class="paymentWidgets"><?php echo esc_attr( $payment_brand ) ?></form>
<div class="overlay">
		    <div class="box-overlay">
		        <a href="#" class="close-button">x</a>
		        <table class="tbloverlay">
		            <tbody>
		                <tr class="trwhatcvc">
		                    <td class="tdWhatCvc" colspan="2" align="center">
		                        <h4><span class="txtWhatCvc"><?php echo esc_attr( __( 'FRONTEND_CC_CVCHELP','wc-zionpayment' ) ) ?></span></h4>
		                    </td>
		                </tr>
		                <tr class="trcards">
		                    <td class="tdCards" width="70%" align="left">
		                        <span class="txtDesCvc"><?php echo esc_attr( __( 'FRONTEND_TT_CVC','wc-zionpayment' ) ) ?></span><br /><br />
		                        <span class="txtDesCvcAmex"><?php echo esc_attr( __( 'FRONTEND_TT_CVCAMEX','wc-zionpayment' ) ) ?></span>
		                    </td>
		                    <td class="imgthreedigits" valign="top">
		                        <img src="<?php echo esc_attr( self::$plugin_url ) ?>/assets/images/cvv_card.png">
		                    </td>
		                </tr>
		            </tbody>
		        </table>
		    </div>
		</div>
