<?php
/**
 * Zionpayment My Payment Information
 *
 * The file is for displaying the Zionpayment My Payment Information
 * Copyright (c) Zionpayment
 *
 * @package     Zionpayment/Templates
 * @located at  /template/ckecmyaccountkout/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<h2 class="header-title"><?php echo esc_attr( __( 'FRONTEND_MC_INFO', 'wc-zionpayment' ) ) ?></h2>

<?php if ( isset( WC()->session->zionpayment_myaccount_error ) ) :?>
	<ul class="response-box error-message">
		<li><?php echo esc_attr( WC()->session->zionpayment_myaccount_error ); ?></li>
	</ul>
<?php
unset( WC()->session->zionpayment_myaccount_error );
endif; ?>

<?php if ( isset( WC()->session->zionpayment_myaccount_success ) ) :?>
	<ul class="response-box success-message">
		<li><?php echo esc_attr( WC()->session->zionpayment_myaccount_success ); ?></li>
	</ul>
<?php
unset( WC()->session->zionpayment_myaccount_success );
endif; ?>


<?php if ( self::$recurring ) :?>

	<?php if ( $is_active_cc ) : ?>
	<div class="group"><?php echo esc_attr( __( 'FRONTEND_MC_CC', 'wc-zionpayment' ) ) ?></div>
	<?php if ( $registered_payments['CC'] ) :?>
		<?php foreach ( $registered_payments['CC'] as $payment ) :?>
		<div class="group-list">
			<div class="group-img">
				<img src="<?php echo esc_attr( self::$plugin_url ) ?>assets/images/<?php echo esc_attr( strtolower( $payment['brand'] ) ) ?>.png" class="card_logo" alt="<?php echo esc_attr( $payment['brand'] ) ?>">
			</div>
			<div class="card_info">
				<?php echo esc_attr( __( 'FRONTEND_MC_ENDING', 'wc-zionpayment' ) ) . ' ' . esc_attr( $payment['last4digits'] ) . '; ' .
				esc_attr( __( 'FRONTEND_MC_VALIDITY', 'wc-zionpayment' ) ) . ' ' . esc_attr( $payment['expiry_month'] ) . '/' . esc_attr( substr( $payment['expiry_year'],-2 ) ) ?>
			</div>
			<div class="clear"></div>
			<div class="group-button">
				<?php if ( $payment['payment_default'] ) :?>
					<button class="btnCustom btnDefault button-primary"><?php echo esc_attr( __( 'FRONTEND_MC_BT_DEFAULT', 'wc-zionpayment' ) ) ?></button>
				<?php else : ?>
					<form action="<?php echo esc_attr( self::$current_url ) ?>page=wc-default" method="post">
						<input type="hidden" name="id" value="<?php echo esc_attr( $payment['id'] ) ?>"/>
						<input type="hidden" name="section" value="zionpayment_ccsaved"/>
						<button class="btnCustom btnDefault button-primary" type="submit" value="submit"><?php echo esc_attr( __( 'FRONTEND_MC_BT_SETDEFAULT', 'wc-zionpayment' ) ) ?></button>
					</form>
				<?php endif; ?>
				<form action="<?php echo esc_attr( self::$current_url ) ?>page=wc-reregister" method="post">
					<input type="hidden" name="id" value="<?php echo esc_attr( $payment['id'] ) ?>"/>
					<input type="hidden" name="section" value="zionpayment_ccsaved"/>
					<button class="btnCustom button-primary" type="submit" value="submit"><?php echo esc_attr( __( 'FRONTEND_MC_BT_CHANGE', 'wc-zionpayment' ) ) ?></button>
				</form>
				<form action="<?php echo esc_attr( self::$current_url ) ?>page=wc-deregister" method="post">
					<input type="hidden" name="id" value="<?php echo esc_attr( $payment['id'] ) ?>"/>
					<input type="hidden" name="section" value="zionpayment_ccsaved"/>
					<button class="btnCustom button-primary" type="submit" value="submit"><?php echo esc_attr( __( 'FRONTEND_MC_BT_DELETE', 'wc-zionpayment' ) ) ?></button>
				</form>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<?php endforeach; ?>
	<?php endif; ?>
	<div class="group-add">
		<form action="<?php echo esc_attr( self::$current_url ) ?>page=wc-register" method="post">
			<input type="hidden" name="section" value="zionpayment_ccsaved"/>
			<button class="btnCustom button-primary" type="submit" value="submit"><?php echo esc_attr( __( 'FRONTEND_MC_BT_ADD', 'wc-zionpayment' ) ) ?></button>
		</form>
	</div>
	<div class="clear"></div>
	<?php endif; ?>

<?php endif; ?>
