<?php
/**
 * Zionpayment Plugin Additional
 *
 * This file is used for add payment status, extra customer info.
 * Copyright (c) Zionpayment
 *
 * @package Zionpayment
 * @located at  /
 */

/**
 * Register New Order Statuses
 * 	(In Review, Pre-Authorization and Payment Accepted)
 */
function register_order_status() {

	register_post_status('wc-in-review', array(
		'label'						=> _x( 'In Review', 'WooCommerce Order Status', 'wc_zionpayment' ),
		'public'					=> true,
		'exclude_from_search'		=> false,
		'show_in_admin_all_list'	=> true,
		'show_in_admin_status_list'	=> true,
		/* translators: %s: https://developer.wordpress.org/rest-api/  */
		'label_count'				=> _n_noop( 'In Review (%s)', 'In Review (%s)', 'wc_zionpayment' ),
	) );

	register_post_status('wc-pre-authorization', array(
		'label'						=> _x( 'Pre-Authorization', 'WooCommerce Order Status', 'wc_zionpayment' ),
		'public'					=> true,
		'exclude_from_search'		=> false,
		'show_in_admin_all_list'	=> true,
		'show_in_admin_status_list'	=> true,
		/* translators: %s: https://developer.wordpress.org/rest-api/  */
		'label_count'				=> _n_noop( 'Pre-Authorization (%s)', 'Pre-Authorization (%s)', 'wc_zionpayment' ),
	) );

	register_post_status('wc-payment-accepted', array(
		'label'						=> _x( 'Payment Accepted', 'WooCommerce Order Status', 'wc_zionpayment' ),
		'public'					=> true,
		'exclude_from_search'		=> false,
		'show_in_admin_all_list'	=> true,
		'show_in_admin_status_list'	=> true,
		/* translators: %s: https://developer.wordpress.org/rest-api/  */
		'label_count'				=> _n_noop( 'Payment Accepted (%s)', 'Payment Accepted (%s)', 'wc_zionpayment' ),
	) );

}
add_filter( 'init', 'register_order_status' );

/**
 * Add New Order Statuses to WooCommerce
 * 	(In Review, Pre-Authorization and Payment Accepted)
 *
 * @param array $order_status order status.
 * @return array
 */
function add_order_status( $order_status ) {

	$order_status['wc-in-review'] = _x( 'In Review', 'In Review Order Status', 'wc_zionpayment' );
	$order_status['wc-pre-authorization'] = _x( 'Pre-Authorization', 'Pre-authorization Order Status', 'wc_zionpayment' );
	$order_status['wc-payment-accepted'] = _x( 'Payment Accepted', 'Payment Accepted Order Status', 'wc_zionpayment' );

	return $order_status;
}
add_filter( 'wc_order_statuses', 'add_order_status' );

/**
 * Add Zionpayment custom order status icon
 */
function vrpay_add_custom_order_status_icon() {
	if ( ! is_admin() ) {
		return;
	}
	?>
		<style>
			.column-order_status mark.pre-authorization:after, .column-order_status mark.payment-accepted:after, .column-order_status mark.in-review:after {
				background-size:100%;
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				text-align: center;
				content: '';
				background-repeat: no-repeat;
			}
			.column-order_status mark.pre-authorization:after {
				background-image: url(<?php echo esc_attr( plugins_url( 'assets/images/pre-authorization.png', __FILE__ ) ); ?>);
			}
			.column-order_status mark.payment-accepted:after {
				background-image: url(<?php echo esc_attr( plugins_url( 'assets/images/payment-accepted.png', __FILE__ ) ); ?>);
			}
			.column-order_status mark.in-review:after {
				background-image: url(<?php echo esc_attr( plugins_url( 'assets/images/in-review.png', __FILE__ ) ); ?>);
			}
		</style>
	<?php
}

add_action( 'wp_print_scripts', 'vrpay_add_custom_order_status_icon' );

