<?php
/**
 * Plugin Name: Zionpayment - WooCommerce
 * Plugin URI:  http://www.bancsabadell.com/
 * Description: WooCommerce with Zionpayment payment gateway
 * Author:      Zionpayment
 * Author URI:  http://www.bancsabadell.com/
 * Version:     1.0.0
 *
 * @package     Zionpayment/Classes
 */

/**
 * Copyright (c) Zionpayment
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

include_once( dirname( __FILE__ ) . '/zionpayment-install.php' );
include_once( dirname( __FILE__ ) . '/zionpayment-additional.php' );
register_activation_hook( __FILE__ , 'zionpayment_activate_plugin' );
register_deactivation_hook( __FILE__ , 'zionpayment_deactivate_plugin' );
add_action( 'plugins_loaded', 'init_payment_gateway', 0 );

ob_start();

define( 'ZIONPAYMENT_VERSION', '1.0.0' );
define( 'ZIONPAYMENT_PLUGIN_FILE', __FILE__ );

/**
 * Check if WooCommerce is active, and if it isn't, disable Zionpayment payments.
 */
function get_notice_woocommerce_activation() {

	echo '<div id="notice" class="error"><p>';
	echo '<a href="http://www.woothemes.com/woocommerce/" style="text-decoration:none" target="_new">WooCommerce </a>' . esc_attr( __( 'BACKEND_GENERAL_PLUGINREQ', 'wc-zionpayment' ) ) . '<b> Zionpayment Payment Gateway for WooCommerce</b>';
	echo '</p></div>';
}

/**
 * Add Configuration Link at Plugin Installation
 *
 * @param array $links links.
 * @return array
 */
function add_configuration_links( $links ) {
	$add_links = array( '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=zionpayment_settings' ) . '">' . __( 'BACKEND_CH_GENERAL','wc-zionpayment' ) . '</a>' );
	return array_merge( $add_links , $links );
}
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'add_configuration_links' );

/**
 * Init payment gateway
 */
function init_payment_gateway() {

	/* loads the Zionpayment language translation strings */
	load_plugin_textdomain( 'wc-zionpayment', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

	if ( ! class_exists( 'WC_Payment_Gateway' ) ) {
		add_action( 'admin_notices', 'get_notice_woocommerce_activation' );
		return;
	}

	include_once( dirname( __FILE__ ) . '/includes/class-zionpayment-general-functions.php' );
	include_once( dirname( __FILE__ ) . '/includes/class-zionpayment-general-models.php' );
	include_once( dirname( __FILE__ ) . '/includes/admin/class-zionpayment-general-settings.php' );
	include_once( dirname( __FILE__ ) . '/includes/admin/class-zionpayment-backend-settings.php' );
	include_once( dirname( __FILE__ ) . '/includes/myaccount/class-zionpayment-payment-information.php' );
	include_once( dirname( __FILE__ ) . '/includes/core/class-zionpayment-payment-core.php' );
	include_once( dirname( __FILE__ ) . '/includes/core/class-zionpayment-version-tracker.php' );

	if ( ! class_exists( 'Zionpayment_Payment_Gateway' ) ) {

		/**
		 * Zionpayment Payment Gateway
		 *
		 * @class Zionpayment_Payment_Gateway
		 */
		class Zionpayment_Payment_Gateway extends WC_Payment_Gateway {
			/**
			 * Payment id
			 *
			 * @var string $payment_id
			 */
			var $payment_id;
			/**
			 * Payment type
			 *
			 * @var string $payment_type
			 */
			var $payment_type;
			/**
			 * Payment brand
			 *
			 * @var string $payment_brand
			 */
			var $payment_brand;
			/**
			 * Payment group
			 *
			 * @var string $payment_group
			 */
			var $payment_group;
			/**
			 * Is recurring
			 *
			 * @var bool $is_recurring
			 */
			var $is_recurring;
			/**
			 * Payment display
			 *
			 * @var string $payment_display
			 */
			protected $payment_display = 'form';
			/**
			 * Languange
			 *
			 * @var string $language
			 */
			var $language;
			/**
			 * Woocommerce order
			 *
			 * @var object
			 */
			protected $wc_order;
			/**
			 * Saved meta boxes
			 *
			 * @var bool $saved_meta_boxes
			 */
			private static $saved_meta_boxes = false;
			/**
			 * Added meta boxes
			 *
			 * @var bool $added_meta_boxes
			 */
			private static $added_meta_boxes = false;
			/**
			 * Added payment method
			 *
			 * @var bool $added_payment_method
			 */
			private static $added_payment_method = false;
			/**
			 * Updated meta boxes
			 *
			 * @var bool $updated_meta_boxes
			 */
			private static $updated_meta_boxes = false;

			/**
			 * Main function
			 *
			 * @return void
			 */
			public function __construct() {
				$this->payment_id   = $this->id;
				$payment_gateway = Zionpayment_General_Functions::get_payment_gateway( $this->payment_id );
				$this->payment_type = $payment_gateway['payment_type'];
				$this->payment_brand = $payment_gateway['payment_brand'];
				$this->payment_group = $payment_gateway['payment_group'];
				$this->is_recurring = $payment_gateway['is_recurring'];
				$this->language     = $payment_gateway['language'];
				$this->plugins_url  = Zionpayment_General_Functions::zn_get_plugin_url();

				$this->form_fields = Zionpayment_Backend_Settings::zn_create_backend_payment_settings( $this->payment_id );
				$this->method_title = Zionpayment_Backend_Settings::zn_get_backend_payment_title( $this->payment_id );
				$this->init_settings();

				// save admin configuration from woocomerce checkout tab.
				add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
				// frontend hook.
				add_action( 'woocommerce_receipt_' . $this->payment_id, array( &$this, 'receipt_page' ) );
				add_action( 'woocommerce_thankyou_' . $this->payment_id, array( &$this, 'thankyou_page' ) );
				// backend hook.
				add_action( 'woocommerce_process_shop_order_meta', array( &$this, 'save_order_meta' ) );
				add_action( 'woocommerce_admin_order_data_after_order_details', array( &$this, 'update_order_status' ) );
				add_action( 'woocommerce_admin_order_data_after_billing_address', array( &$this, 'add_payment_method' ) );
				add_action( 'woocommerce_admin_order_data_after_shipping_address', array( &$this, 'add_additional_information' ) );

				// enable woocommerce refund for {payment gateway}.
				$this->supports = array( 'refunds' );

				if ( isset( WC()->session->zionpayment_thankyou_page ) ) {
					unset( WC()->session->zionpayment_thankyou_page );
				}
				if ( isset( WC()->session->zionpayment_receipt_page ) ) {
					unset( WC()->session->zionpayment_receipt_page );
				}

			}

			/**
			 * Get Payment Logo(s)
			 * (extended at Zionpayment payment gateways class)
			 *
			 * @return string
			 */
			protected function zn_get_payment_logo() {

				return false;
			}

			/**
			 * From class WC_Payment_Gateway
			 * Available payment gateway at frontend.
			 *
			 * @return boolean
			 */
			public function is_available() {

				$is_available = parent::is_available();
				return $is_available;
				// if ( $is_available ) {

				// 	$recurring = get_option( 'zionpayment_general_recurring' );
				// 	switch ( $this->payment_id ) {
				// 		case 'zionpayment_cc':
				// 			if ( $recurring && get_current_user_id() > 0 ) {
				// 				$is_available = false;
				// 			}
				// 			break;
				// 		case 'zionpayment_ccsaved':
				// 			if ( ! $recurring || get_current_user_id() === 0 ) {
				// 				$is_available = false;
				// 			}
				// 			break;
				// 		default:
				// 			$is_available = true;
				// 			break;
				// 	}
				// }
				// return $is_available;
			}

			/**
			 * Check if current user login is admin
			 *
			 * @return boolean
			 */
			private function is_site_admin() {
				return in_array( 'administrator',  wp_get_current_user()->roles, true );
			}

			/**
			 * Get title/label of payment method based on user login
			 *
			 * @return string
			 */
			public function get_title() {
				if ( $this->is_site_admin() ) {
					return Zionpayment_General_Functions::zn_translate_backend_payment( $this->payment_id );
				}
				return Zionpayment_General_Functions::zn_translate_frontend_payment( $this->payment_id );
			}

			/**
			 * From class WC_Payment_Gateway
			 * Payment gateway icon.
			 * (extended at Zionpayment payment gateways class)
			 *
			 * @return string
			 */
			public function get_icon() {

				$title = Zionpayment_General_Functions::zn_translate_frontend_payment( $this->payment_id );
				$icon_html = '';
				$icon_html .= '<img src="' . $this->zn_get_payment_logo() . '" alt="' . $title . '" title="' . $title . '" style="height:40px; margin:0 10px 20px 0" />';

				return apply_filters( 'woocommerce_gateway_icon', $icon_html, $this->payment_id );
			}

			/**
			 * Get multi icon (brands)
			 *
			 * @return string
			 */
			public function zn_get_multi_icon() {

				$icon_html = '';

				$payment_setting = get_option( 'woocommerce_' . $this->payment_id . '_settings' );
				$cards = $payment_setting['card_types'];
				if ( isset( $cards ) && '' !== $cards ) {
					foreach ( $cards as $card ) {

						$icon = $this->plugins_url . '/assets/images/' . strtolower( $card ) . '.png';
						$icon_html .= '<img src="' . $icon . '" alt="' . strtolower( $card ) . '" title="' . strtolower( $card ) . '" style="height:40px; margin:0 5px 5px 0" />';
					}
				}

				return $icon_html;
			}

			/**
			 * Process The Payment and Return The Result
			 *
			 * @param int $order_id order id.
			 * @return array
			 */
			public function process_payment( $order_id ) {

				$this->wc_order = new WC_Order( $order_id );
				return array(
					'result'   => 'success',
					'redirect' => $this->wc_order->get_checkout_payment_url( true ),
				);
			}

			/**
			 * Create Payment Gateways Form (form / redirect)
			 * Calls from hook "woocommerce_receipt_{gateway_id}"
			 *
			 * @param int $order_id order id.
			 * @return void
			 */
			public function receipt_page( $order_id ) {

				if ( ! isset( WC()->session->zionpayment_receipt_page ) ) {
					$this->zn_set_payment_form( $order_id );
					WC()->session->set( 'zionpayment_receipt_page', true );
				}
				$id = Zionpayment_General_Functions::zn_get_request_value( 'id' );
				if ( $id ) {
					$this->zn_response_page( $order_id );
				}
			}

			/**
			 * Set Payment Form Checkout
			 *
			 * @param int $order_id order id.
			 * @return void
			 */
			private function zn_set_payment_form( $order_id ) {

				global $wp;
				$is_one_click_payments = false;

				$payment_parameters = $this->zn_get_payment_parameters( $order_id );

				$checkout_result = Zionpayment_Payment_Core::get_checkout_result($payment_parameters);

				$url_config['payment_widget'] = Zionpayment_Payment_Core::get_payment_widget_url( $payment_parameters, $checkout_result['id'] );

				if ( isset( $wp->request ) ) {
					$url_config['return_url'] = home_url( $wp->request ) . '/?key=' . Zionpayment_General_Functions::zn_get_request_value( 'key' );
				} else {
					$url_config['return_url'] = get_page_link() . '&order-pay=' . Zionpayment_General_Functions::zn_get_request_value( 'order-pay' ) . '&key=' . Zionpayment_General_Functions::zn_get_request_value( 'key' );
				}

				$url_config['cancel_url'] = WC()->cart->get_checkout_url();


				if ( ! empty( $payment_parameters['registrations'] ) ) {
					$is_one_click_payments = true;
				}

				$payment_widget_content = Zionpayment_Payment_Core::get_payment_widget_content( $url_config['payment_widget'], $payment_parameters['server_mode'] );

				if ( strpos( $payment_widget_content, 'errorDetail' ) !== false ) {
					$this->zn_do_error_payment( $order_id, 'wc-canceled', 'ERROR_GENERAL_REDIRECT' );
				}

				$payment_form = Zionpayment_General_Functions::zn_get_payment_form( $this->payment_id );

				wp_enqueue_script( 'zionpayment_formpayment_script', $url_config['payment_widget'], array(), null );
				wp_enqueue_style( 'zionpayment_formpayment_style', $this->plugins_url . '/assets/css/formpayment.css', array(), null );

				switch ( $payment_form ) {
					case 'redirect':
						include( dirname( __FILE__ ) . '/templates/checkout/template-redirect-payment.php' );
						break;
					default:
						include( dirname( __FILE__ ) . '/templates/checkout/template-payment-form.php' );
						break;
				}
			}

			/**
			 * Get Payment Gateways Response Page
			 *
			 * @param int $order_id order id.
			 * @return void
			 */
			private function zn_response_page( $order_id ) {
				$is_testmode_available = false;
				$payment_parameters = Zionpayment_General_Functions::zn_get_credentials( $this->payment_id, $is_testmode_available );
				$payment_response = Zionpayment_Payment_Core::get_payment_response( Zionpayment_General_Functions::zn_get_request_value( 'id' ), $payment_parameters );

				if ( ! $payment_response ) {
					$this->zn_do_error_payment( $order_id, 'wc-failed', 'ERROR_GENERAL_NORESPONSE' );
				} else {
					if ( isset( $payment_response['amount'] ) ) {
						$payment_amount = $payment_response['amount'];
					}

					$payment_status = Zionpayment_Payment_Core::get_payment_status_by_result_code( $payment_response['result']['code'] );

					if ( 'ACK' === $payment_status ) {
						if ( $this->is_recurring ) {
							$account = Zionpayment_General_Functions::zn_get_account_by_payment_response( $this->payment_id, $payment_response );
							$this->zn_save_recurring_payment( $order_id, $payment_response, $account );
						}

						$order_status = $this->zn_get_success_order_status( $this->payment_id, $payment_response['result']['code'] );
						$this->zn_do_success_payment( $order_id, $payment_response, $order_status );
					} else {
						$this->zn_do_error_response( $order_id, $payment_response, $payment_status );
					}
				}
			}

			/**
			 * Succes Payment Action
			 *
			 * @param int    $order_id order id.
			 * @param array  $payment_response payment Response.
			 * @param string $order_status order status.
			 * @return void
			 */
			private function zn_do_success_payment( $order_id, $payment_response, $order_status ) {

				$this->wc_order = new WC_Order( $order_id );

				$merchant_info = $this->zn_get_merchant_info();
				Zionpayment_Version_Tracker::send_version_tracker( $merchant_info );

				$reference_id = $payment_response['id'];
				$this->zn_save_transactions( $order_id, $payment_response, $reference_id, $order_status );

				// Update Order Status.
				$this->wc_order->update_status( 'wc-processing', 'order_note' );
				$this->wc_order->update_status( $order_status, 'order_note' );
				// Empty awaiting payment session.
				if ( ! empty( WC()->session->order_awaiting_payment ) ) {
					unset( WC()->session->order_awaiting_payment );
				}
				// Reduce stock levels.
				$this->reduce_order_stock();
				// Remove cart.
				WC()->cart->empty_cart();
				wp_safe_redirect( $this->get_return_url( $this->wc_order ) );
				exit();
			}

			/**
			 * Reduce the order stock
			 *
			 * @return void
			 */
			protected function reduce_order_stock() {
				if ( $this->is_version_greater_than( '3.0' ) ) {
					wc_reduce_stock_levels( $this->wc_order->get_order_number() );
				} else {
					$this->wc_order->reduce_order_stock();
				}
			}

			/**
			 * Error Response Action
			 *
			 * @param int    $order_id order id.
			 * @param array  $payment_response payment status.
			 * @param string $payment_status payment status.
			 * @return void
			 */
			private function zn_do_error_response( $order_id, $payment_response, $payment_status ) {

				if ( 'NOK' === $payment_status ) {
					$error_identifier = Zionpayment_Payment_Core::get_error_identifier( $payment_response['result']['code'] );
				} else {
					$error_identifier = 'ERROR_UNKNOWN';
				}

				$order_status = 'wc-failed';
				$this->zn_save_transactions( $order_id, $payment_response, $payment_response['id'], $order_status );
				$this->zn_do_error_payment( $order_id, $order_status, $error_identifier );
			}

			/**
			 * Error Payment Action
			 *
			 * @param int    $order_id order id.
			 * @param string $order_status order status.
			 * @param string $error_identifier error identifier.
			 * @return void
			 */
			private function zn_do_error_payment( $order_id, $order_status, $error_identifier ) {

				$this->wc_order = new WC_Order( $order_id );

				$error_translated = Zionpayment_General_Functions::zn_translate_error_identifier( $error_identifier );

				// Cancel the order.
				$this->wc_order->cancel_order( $error_translated );
				$this->wc_order->update_status( $order_status, 'order_note' );
				// To display failure messages from woocommerce session.
				global $woocommerce;
				$woocommerce->session->errors = $error_translated;

				wc_add_notice( $error_translated, 'error' );

				wp_safe_redirect( WC()->cart->get_checkout_url() );
				exit();
			}

			/**
			 * Thankyou Page
			 * Calls from hook "woocommerce_thankyou_{gateway_id}"
			 */
			public function thankyou_page() {
				if ( ! isset( WC()->session->zionpayment_thankyou_page ) ) {
					WC()->session->set( 'zionpayment_thankyou_page', true );
				}
			}

			/**
			 * [BACKEND] Save Order Meta Boxes
			 * Change Payment Status & Save Backend Order
			 * Calls from hook "woocommerce_process_shop_order_meta"
			 *
			 * @return void
			 */
			public function save_order_meta() {

				if ( ! self::$saved_meta_boxes ) {

					$original_post_status = Zionpayment_General_Functions::zn_get_request_value( 'original_post_status', '' );
					$auto_draft = Zionpayment_General_Functions::zn_get_request_value( 'auto_draft' );

					if ( 'auto-draft' === $original_post_status && '1' === $auto_draft ) {
						$this->zn_save_backend_order();
					} else {
						$order_id = (int) Zionpayment_General_Functions::zn_get_request_value( 'post_ID', '' );
						$is_zionpayment_payment = $this->zn_is_zionpayment_by_order_id( $order_id );
						if ( $is_zionpayment_payment ) {
							$this->zn_change_order_status();
						}
					}
					self::$saved_meta_boxes = true;
				}
			}

			/**
			 * [BACKEND] Change Order Status at backend
			 *
			 * @return void
			 */
			private function zn_change_order_status() {

				$order_id = (int) Zionpayment_General_Functions::zn_get_request_value( 'post_ID', '' );
				$original_post_status = Zionpayment_General_Functions::zn_get_request_value( 'original_post_status', '' );
				$order_post_status = Zionpayment_General_Functions::zn_get_request_value( 'order_status', '' );

				if ( 'wc-in-review' === $original_post_status && 'wc-in-review' === $order_post_status ) {
					$this->zn_update_order_status( $order_id, $original_post_status );
				} elseif ( 'wc-pre-authorization' === $original_post_status && 'wc-payment-accepted' === $order_post_status ) {
					$backoffice_config['payment_type'] = 'CP';
					$backoffice_config['order_status'] = $order_post_status;
					$backoffice_config['error_message'] = __( 'ERROR_GENERAL_CAPTURE_PAYMENT', 'wc-zionpayment' );
					$this->zn_do_backoffice_payment( $order_id, $backoffice_config );
				} elseif ( 'wc-payment-accepted' === $original_post_status && 'wc-refunded' === $order_post_status ) {
					$backoffice_config['payment_type'] = 'RF';
					$backoffice_config['order_status'] = $order_post_status;
					$backoffice_config['error_message'] = __( 'ERROR_GENERAL_REFUND_PAYMENT', 'wc-zionpayment' );
					$this->zn_do_backoffice_payment( $order_id, $backoffice_config );
				} elseif ( 'wc-payment-accepted' !== $original_post_status && 'wc-refunded' === $order_post_status ) {
					$redirect = get_admin_url() . 'post.php?post=' . $order_id . '&action=edit';
					wp_safe_redirect( $redirect );
					exit;
				}

			}

			/**
			 * Is Zionpayment Payment
			 * validate payment module from order id
			 *
			 * @param int $order_id order id.
			 * @return boolean
			 */
			private function zn_is_zionpayment_by_order_id( $order_id ) {

				$transaction_log = Zionpayment_General_Models::zn_get_db_transaction_log( $order_id );
				$payment_id = $transaction_log['payment_id'];

				return $this->zn_is_zionpayment_by_payment_id( $payment_id );
			}

			/**
			 * Is Zionpayment Payment
			 * validate payment module from order id
			 *
			 * @param string $payment_id payment method.
			 * @return boolean
			 */
			private function zn_is_zionpayment_by_payment_id( $payment_id ) {

				if ( strpos( $payment_id, 'zionpayment' ) !== false ) {
					return true;
				}
				return false;
			}

			/**
			 * [BACKEND] Update Payment Status from gateway
			 *
			 * @param int     $order_id order id.
			 * @param boolean $original_post_status original post status.
			 */
			private function zn_update_order_status( $order_id, $original_post_status = false ) {

				$transaction_log = Zionpayment_General_Models::zn_get_db_transaction_log( $order_id );

				if ( ! $original_post_status ) {
					$original_post_status = $transaction_log['order_status'];
				}

				$backoffice_parameter = Zionpayment_General_Functions::zn_get_credentials( $transaction_log['payment_id'] );

				$backoffice_xml_result = Zionpayment_Payment_Core::update_status( $transaction_log['reference_id'], $backoffice_parameter );
				$backoffice_result = simplexml_load_string( $backoffice_xml_result );

				if ( isset( $backoffice_result->Result ) ) {
					$backoffice_result_code = (string) $backoffice_result->Result->Transaction->Processing->Return['code'];
				} else {
					$backoffice_result_code = '';
				}

				$payment_status = Zionpayment_Payment_Core::get_payment_status_by_result_code( $backoffice_result_code );

				if ( 'ACK' === $payment_status && ! Zionpayment_Payment_Core::is_success_review( $backoffice_result_code ) ) {

					$payment_code = (string) $backoffice_result->Result->Transaction->Payment['code'];
					$payment_type = substr( $payment_code,-2 );
					if ( 'PA' === $payment_type ) {
						$order_status = 'wc-pre-authorization';
					} elseif ( 'DB' === $payment_type ) {
						$order_status = 'wc-payment-accepted';
					}

					Zionpayment_General_Models::zn_update_db_transaction_log_status( $order_id, $order_status );
					Zionpayment_General_Models::zn_update_db_posts_status( $order_id, $order_status );
					if ( $original_post_status !== $order_status ) {
						$wc_order_status = wc_get_order_statuses();
						$status_name['original'] = $wc_order_status[ $original_post_status ];
						$status_name['new'] = $wc_order_status[ $order_status ];
						$this->zn_add_order_comments( $order_id, $status_name );
					}
				}

			}

			/**
			 * [BACKEND] Save Backend Order
			 * Add order from backend with registered payment
			 *
			 * @return void
			 */
			private function zn_save_backend_order() {

				$order['order_id'] = Zionpayment_General_Functions::zn_get_request_value( 'post_ID', '' );
				$registration_id = Zionpayment_General_Functions::zn_get_request_value( '_payment_recurring', '' );
				$payment_method = Zionpayment_General_Functions::zn_get_request_value( '_payment_method', '' );

				$is_zionpayment_payment = $this->zn_is_zionpayment_by_payment_id( $payment_method );

				if ( $is_zionpayment_payment ) {

					$registered_payment = Zionpayment_General_Models::zn_get_db_registered_payment_by_registration_id( $registration_id );
					$order['payment_id'] = Zionpayment_General_Functions::zn_get_payment_id_by_group( $registered_payment['payment_group'] );
					$order['user_id'] = $registered_payment['cust_id'];
					$order['payment_brand'] = $registered_payment['brand'];

					$backend_order_parameter = $this->zn_get_backend_order_parameter( $order );
					$payment_response = Zionpayment_Payment_Core::pay_use_registered_account( $registration_id, $backend_order_parameter );
					$payment_status = Zionpayment_Payment_Core::get_payment_status_by_result_code( $payment_response['result']['code'] );

					if ( 'ACK' === $payment_status ) {
						$order_status = $this->zn_get_success_order_status( $order['payment_id'], $payment_response['result']['code'] );
						$this->zn_do_success_backend_order( $order, $payment_response, $order_status );
					} else {
						$order_status = 'wc-failed';
						Zionpayment_General_Models::zn_update_db_posts_status( (int)$order['order_id'], $order_status );
					}

					$_POST['order_status'] = $order_status;
					$_POST['_payment_method'] = $order['payment_id'];
				}

			}

			/**
			 * [BACKEND] Get Backend Order Paramer
			 * Get parameter for backend order
			 *
			 * @param array $order order.
			 * @return array
			 */
			private function zn_get_backend_order_parameter( $order ) {

				$backend_order_parameter = array();
				$backend_order_parameter = Zionpayment_General_Functions::zn_get_credentials( $order['payment_id'] );

				$order_detail = Zionpayment_General_Models::zn_get_db_order_detail( $order['order_id'] );
				foreach ( $order_detail as $value ) {
					if ( '_order_total' === $value['meta_key'] ) {
						$backend_order_parameter['amount'] = $value['meta_value'];
					} elseif ( '_order_currency' === $value['meta_key'] ) {
						$backend_order_parameter['currency'] = $value['meta_value'];
					}
				}

				$backend_order_parameter['transaction_id'] = $order['order_id'];
				$backend_order_parameter['payment_type'] = Zionpayment_General_Functions::zn_get_payment_type( $order['payment_id'] );
				$backend_order_parameter['payment_recurring'] = 'REPEATED';

				return $backend_order_parameter;
			}

			/**
			 * [BACKEND] Do Success Backend Order
			 * save order & status from backend order if success
			 *
			 * @param array $order order.
			 * @param array $payment_response payment response.
			 * @param string $order_status payment parameter.
			 * @return void
			 */
			private function zn_do_success_backend_order( $order, $payment_response, $order_status ) {

				$this->zn_add_customer_note( $order['payment_id'], $order['order_id'], $payment_response['currency'] );

				$this->reduce_order_stock();

				$transaction['order_id'] = $order['order_id'];
				$transaction['payment_type'] = Zionpayment_General_Functions::zn_get_payment_type( $order['payment_id'] );
				$transaction['reference_id'] = $payment_response['id'];
				$transaction['payment_brand'] = $order['payment_brand'];
				$transaction['transaction_id'] = $payment_response['merchantTransactionId'];
				$transaction['payment_id'] = $order['payment_id'];
				$transaction['order_status'] = $order_status;
				$transaction['amount'] = $payment_response['amount'];
				$transaction['currency'] = $payment_response['currency'];
				$transaction['customer_id'] = $order['user_id'];

				Zionpayment_General_Models::zn_save_db_transaction( $transaction );
				Zionpayment_General_Models::zn_update_db_posts_status( $transaction['transaction_id'], $order_status );

			}

			/**
			 * [BACKEND] From class WC_Payment_Gateway
			 * Payment Refund from button refund.
			 *
			 * @param int    $order_id order id.
			 * @param float  $amount amount.
			 * @param string $reason reason.
			 * @return boolean
			 */
			public function process_refund( $order_id, $amount = null, $reason = '' ) {

				$is_zionpayment_payment = $this->zn_is_zionpayment_by_order_id( $order_id );

				if ( $is_zionpayment_payment ) {
					$woocommerce_refund = true;
					$backoffice_config['payment_type'] = 'RF';
					$backoffice_config['order_status'] = 'wc-refunded';
					$backoffice_config['error_message'] = __( 'ERROR_GENERAL_REFUND_PAYMENT', 'wc-zionpayment' );

					$refund_status = $this->zn_do_backoffice_payment( $order_id, $backoffice_config, $woocommerce_refund );

					if ( $refund_status ) {
						$this->wc_order = new WC_Order( $order_id );
						$this->wc_order->update_status( 'wc-refunded', 'order_note' );
						Zionpayment_General_Models::zn_update_db_transaction_log_status( $order_id, 'wc-refunded' );
					}
					return $refund_status;
				}
				return false;
			}

			/**
			 * [BACKEND] Capture / Refund Payment
			 * do backoffice operations (capture/refund)
			 *
			 * @param int   $order_id order id.
			 * @param array $backoffice_config back office config.
			 * @param bool  $woocommerce_refund woocommerce refund.
			 * @return boolean ( if @param $woocommerce_refund = true )
			 */
			private function zn_do_backoffice_payment( $order_id, $backoffice_config, $woocommerce_refund = false ) {

				$transaction_log = Zionpayment_General_Models::zn_get_db_transaction_log( $order_id );
				$is_testmode_available = true;
				$is_multichannel_available = true;

				$backoffice_parameter = Zionpayment_General_Functions::zn_get_credentials( $transaction_log['payment_id'], $is_testmode_available, $is_multichannel_available );
				$backoffice_parameter['amount'] = $transaction_log['amount'];
				$backoffice_parameter['currency'] = $transaction_log['currency'];
				$backoffice_parameter['payment_type'] = $backoffice_config['payment_type'];

				$backoffice_operation_response = Zionpayment_Payment_Core::backoffice_operation( $transaction_log['reference_id'], $backoffice_parameter );
				$payment_status = Zionpayment_Payment_Core::get_payment_status_by_result_code( $backoffice_operation_response['result']['code'] );

				if ( 'ACK' === $payment_status ) {
					Zionpayment_General_Models::zn_update_db_transaction_log_status( $order_id, $backoffice_config['order_status'] );
					if ( $woocommerce_refund ) {
						return true;
					}
				} else {
					if ( $woocommerce_refund ) {
						return false;
					} else {
						$redirect = get_admin_url() . 'post.php?post=' . $order_id . '&action=edit';
						WC_Admin_Meta_Boxes::add_error( $backoffice_config['error_message'] );
						wp_safe_redirect( $redirect );
						exit;
					}
				}
				return false;
			}

			/**
			 * [BACKEND] Set Additional Information
			 * add additional information on Edit Order Page
			 * Calls from hook "woocommerce_admin_order_data_after_shipping_address"
			 *
			 * @return void
			 */
			public function add_additional_information() {

				if ( ! self::$added_meta_boxes ) {
					$order_id = (int) Zionpayment_General_Functions::zn_get_request_value( 'post' );

					$is_zionpayment_payment = $this->zn_is_zionpayment_by_order_id( $order_id );
					if ( $is_zionpayment_payment ) {
						$transaction_log = Zionpayment_General_Models::zn_get_db_transaction_log( $order_id );
						if ( $transaction_log['additional_information'] ) {
							$additional_information = $this->zn_get_additional_info( $transaction_log['additional_information'] );
						}
						include( dirname( __FILE__ ) . '/templates/admin/meta-boxes/template-additional-information.php' );
					}
					self::$added_meta_boxes = true;
				}
			}

			/**
			 * [BACKEND] Update Order Status
			 * update order status from gateway
			 * Calls from hook "woocommerce_admin_order_data_after_order_details"
			 *
			 * @return void
			 */
			public function update_order_status() {

				$post_type = false;
				if ( isset( $_GET['post_type'] ) ) { // input var okay.
					$post_type = wp_verify_nonce( sanitize_text_field( wp_unslash( $_GET['post_type'] ) ) ); // input var okay.
				}

				if ( ! self::$updated_meta_boxes && 'shop_order' !== $post_type ) {

					$order_id = (int) Zionpayment_General_Functions::zn_get_request_value( 'post' );
					$is_zionpayment_payment = $this->zn_is_zionpayment_by_order_id( $order_id );

					if ( $is_zionpayment_payment ) {

						$order = wc_get_order( $order_id );
						if ( 'in-review' === $order->get_status() ) {

							$request_section = Zionpayment_General_Functions::zn_get_request_value( 'section', '' );

							if ( $order_id && 'update-order' === $request_section ) {
								$this->zn_update_order_status( $order_id );
								$redirect = get_admin_url() . 'post.php?post=' . $order_id . '&action=edit';
								wp_safe_redirect( $redirect );
								exit;
							}

							include( dirname( __FILE__ ) . '/templates/admin/meta-boxes/template-update-order.php' );
						}
					}
					self::$updated_meta_boxes = true;
				}
			}

			/**
			 * [BACKEND] Add Payment Method
			 * Add Payment Method (Recurring) at Backend Order
			 * Calls from hook "woocommerce_admin_order_data_after_billing_address"
			 *
			 * @return void
			 */
			public function add_payment_method() {
				if ( ! self::$added_payment_method ) {
					$action = Zionpayment_General_Functions::zn_get_request_value( 'action', 'false' );
					$order_id = Zionpayment_General_Models::zn_get_db_last_order_id();
					if ( $order_id ) {
						include( dirname( __FILE__ ) . '/templates/admin/meta-boxes/template-add-payment-method.php' );
					}

					self::$added_payment_method = true;
				}

			}

			/**
			 * Get Success Payment Status
			 *
			 * @param string $payment_id payment id.
			 * @param string $result_code result code.
			 * @return string
			 */
			private function zn_get_success_order_status( $payment_id, $result_code ) {

				$is_success_review = Zionpayment_Payment_Core::is_success_review( $result_code );

				if ( $is_success_review ) {
					$order_status = 'wc-in-review';
				} else {

					if ( 'PA' === Zionpayment_General_Functions::zn_get_payment_type( $payment_id ) ) {
						$order_status = 'wc-pre-authorization';
					} else {
						$order_status = 'wc-payment-accepted';
					}
				}

				return $order_status;
			}

			/**
			 * Add Customer Notes
			 *
			 * @param string $payment_id payment id.
			 * @param int    $order_id order id.
			 * @param string $currency currency.
			 */
			private function zn_add_customer_note( $payment_id, $order_id, $currency ) {
				$this->wc_order = new WC_Order( $order_id );

				$new_line = "\n";
				$transaction_id_title = __( 'BACKEND_TT_TRANSACTION_ID', 'wc-zionpayment' );

				$payment_comments  = Zionpayment_General_Functions::zn_translate_frontend_payment( $payment_id ) . $new_line;

				$customer_note = $this->get_wc_order_property_value( 'customer_note' );
				if ( $customer_note ) {
					$customer_note .= $new_line;
				}

				$customer_note .= html_entity_decode( $payment_comments, ENT_QUOTES, 'UTF-8' );

				$this->set_wc_order_property_value( 'customer_note', $customer_note );

				$order_notes = array(
						'ID'            => $this->wc_order->get_order_number(),
						'post_excerpt'  => $this->get_wc_order_property_value( 'customer_note' ),
				);
				wp_update_post( $order_notes );

			}

			/**
			 * Set wc order property value
			 *
			 * @param  string $property property of class WC_Order.
			 * @param  mixed  $value value.
			 * @return void
			 */
			protected function set_wc_order_property_value( $property, $value ) {
				if ( $this->is_version_greater_than( '3.0' ) ) {
					$function = 'set_' . $property;
					$this->wc_order->$function( $value );
				} else {
					$value = $this->wc_order->$property;
				}
			}

			/**
			 * Get Cart Item Parameters
			 *
			 * @return array
			 */
			private function zn_get_cart_items() {

				global $woocommerce;
				$wc_cart = $woocommerce->cart;
				$count = 0;
				$cart_items = array();
				foreach ( $wc_cart->get_cart() as $cart ) {

					$cart_items[ $count ]['merchant_item_id'] = $cart['product_id'];
					$cart_items[ $count ]['discount'] = Zionpayment_General_Functions::zn_get_payment_discount_in_percent( $cart );
					$cart_items[ $count ]['quantity'] = (int) $cart['quantity'];
					$cart_items[ $count ]['name'] = $cart['data']->post->post_title;
					$cart_items[ $count ]['price'] = Zionpayment_General_Functions::zn_get_payment_price_with_tax_and_discount( $cart );
					$cart_items[ $count ]['tax'] = Zionpayment_General_Functions::zn_get_payment_tax_in_percent( $cart );

					$count++;
				}

				return $cart_items;
			}

			/**
			 * Get Customer Parameters for Order
			 *
			 * @param int $order_id order id.
			 * @return array
			 */
			private function zn_get_customer_parameters( $order_id ) {

				$this->wc_order = new WC_Order( $order_id );

				$customer['first_name']   = $this->get_wc_order_property_value( 'billing_first_name' );
				$customer['last_name']    = $this->get_wc_order_property_value( 'billing_last_name' );
				$customer['email']        = $this->get_wc_order_property_value( 'billing_email' );
				$customer['phone']        = $this->get_wc_order_property_value( 'billing_phone' );

				return $customer;
			}

			/**
			 * Get Billing Parameters for Order
			 *
			 * @param int $order_id order id.
			 * @return array
			 */
			private function zn_get_billing_parameters( $order_id ) {

				$this->wc_order = new WC_Order( $order_id );

				$billing['street']        = $this->get_wc_order_property_value( 'billing_address_1' ) . ', ' . $this->get_wc_order_property_value( 'billing_address_2' );
				$billing['city']          = $this->get_wc_order_property_value( 'billing_city' );
				$billing['zip']           = $this->get_wc_order_property_value( 'billing_postcode' );
				$billing['country_code']  = $this->get_wc_order_property_value( 'billing_country' );

				return $billing;
			}

			/**
			 * Get Recurring Parameter for Order
			 *
			 * @param int $order_id order id.
			 * @return array
			 */
			private function zn_get_recurring_parameters( $order_id ) {

				$this->wc_order = new WC_Order( $order_id );

				$recurring = array();
				$recurring['registrations'] = $this->zn_get_registrations_parameter();
				$recurring['payment_registration'] = 'true';
				if ( 'zionpayment_ccsaved' === $this->payment_id ) {
					$recurring['3D']['amount'] = $this->wc_order->get_total();
					$recurring['3D']['currency'] = get_woocommerce_currency();
				}

				return $recurring;
			}

			/**
			 * Get Payment Parameters for Order
			 *
			 * @param int $order_id order id.
			 * @return array
			 */
			private function zn_get_payment_parameters( $order_id ) {

				$this->wc_order = new WC_Order( $order_id );

				$payment                             = Zionpayment_General_Functions::zn_get_credentials( $this->payment_id );
				$payment['transaction_id']           = $this->wc_order->get_order_number() . time();
				$payment['amount']                   = $this->wc_order->get_total();
				$payment['currency']                 = get_woocommerce_currency();
				$payment['customer']                 = $this->zn_get_customer_parameters( $order_id );
				$payment['billing']                  = $this->zn_get_billing_parameters( $order_id );

				$payment['payment_type']             = $this->payment_type;
				$payment['payment_brand']            = $this->payment_brand;

				if ( $this->is_recurring ) {
					$recurring_parameter = $this->zn_get_recurring_parameters( $order_id );
					$payment = array_merge( $payment, $recurring_parameter );
				}

				$payment['customer_ip'] = Zionpayment_General_Functions::get_customer_ip();

				return $payment;
			}

			/**
			 * Is woocommerce version greater than
			 *
			 * @param  int $version woocommerce veersion.
			 * @return boolean
			 */
			protected static function is_version_greater_than( $version ) {
				if ( class_exists( 'WooCommerce' ) ) {
					global $woocommerce;
					if ( version_compare( $woocommerce->version, $version, '>=' ) ) {
						return true;
					}
				}
				return false;
			}

			/**
			 * Get wc order property value
			 *
			 * @param  string $property property of class WC_Order.
			 * @return mixed
			 */
			protected function get_wc_order_property_value( $property ) {
				if ( $this->is_version_greater_than( '3.0' ) ) {
					$function = 'get_' . $property;
			   		return $this->wc_order->$function();
			  	}
				return $this->wc_order->$property;
			}

			/**
			 * Get Additional Information
			 *
			 * @param serialize $serialize_info serialize info.
			 * @return array
			 */
			private function zn_get_additional_info( $serialize_info ) {
				$additional_info = array();
				if ( $serialize_info ) {
					$unserialize_info = maybe_unserialize( $serialize_info );
					foreach ( $unserialize_info as $info ) {
						$explode_info = explode( '=>',$info );
						$additional_info[ $explode_info[0] ] = $explode_info[1];
					}
				}

				return $additional_info;
			}

			/**
			 * Get Registered Payments for One Click Checkout Order
			 *
			 * @return array
			 */
			private function zn_get_registrations_parameter() {

				$registration = array();
				$credentials = Zionpayment_General_Functions::zn_get_credentials( $this->payment_id );
				$registered_payments = Zionpayment_General_Models::zn_get_db_registered_payment( $this->payment_group, $credentials );

				foreach ( $registered_payments as $key => $registrations ) {
					$registration[ $key ] = $registrations['registration_id'];
				}

				return $registration;
			}

			/**
			 * Save Recurring Payment from Pay and Save Order
			 *
			 * @param int   $order_id order id.
			 * @param array $payment_response payment status.
			 * @param array $account account.
			 * @return void
			 */
			private function zn_save_recurring_payment( $order_id, $payment_response, $account ) {

				$is_registered_payment = Zionpayment_General_Models::zn_is_registered_payment( $payment_response['registrationId'] );

				if ( ! $is_registered_payment ) {
					$credentials = Zionpayment_General_Functions::zn_get_credentials( $this->payment_id );
					$registered_payment = $account;
					$registered_payment['payment_group'] = $this->payment_group;
					$registered_payment['payment_brand'] = $payment_response['paymentBrand'];
					$registered_payment['server_mode'] = $credentials['server_mode'];
					$registered_payment['channel_id'] = $credentials['channel_id'];
					$registered_payment['registration_id'] = $payment_response['registrationId'];
					$registered_payment['payment_default'] = Zionpayment_General_Models::zn_get_db_payment_default( $this->payment_group, $credentials );

					Zionpayment_General_Models::zn_save_db_registered_payment( $registered_payment );

				}
			}

			/**
			 * Save Order Transaction Details
			 *
			 * @param int    $order_id order id.
			 * @param array  $payment_response payment response.
			 * @param int    $reference_id reference id.
			 * @param string $order_status order status.
			 * @return void
			 */
			private function zn_save_transactions( $order_id, $payment_response, $reference_id, $order_status ) {

				$this->wc_order = new WC_Order( $order_id );

				$transaction = array();

				if ( isset( $payment_response['currency'] ) ) {
					$this->zn_add_customer_note( $this->payment_id, $order_id, $payment_response['currency'] );
				}

				$additional_info = '';

				if ( empty( $payment_response['paymentBrand'] ) ) {
					$payment_response['paymentBrand'] = $this->payment_brand;
				}

				$transaction['order_id'] = $this->wc_order->get_order_number();
				$transaction['payment_type'] = $this->payment_type;
				$transaction['reference_id'] = $reference_id;
				$transaction['payment_brand'] = $payment_response['paymentBrand'];

				if ( isset( $payment_response['merchantTransactionId'] ) ) {
					$transaction['transaction_id'] = $payment_response['merchantTransactionId'];
				}

				$transaction['payment_id'] = $this->payment_id;
				$transaction['order_status'] = $order_status;
				$transaction['amount'] = $this->wc_order->get_total();
				$transaction['currency'] = get_woocommerce_currency();
				$transaction['customer_id'] = ($this->wc_order->get_user_id()) ? $this->wc_order->get_user_id()  : 0;

				Zionpayment_General_Models::zn_save_db_transaction( $transaction, $additional_info );
			}

			/**
			 * Add Order comments
			 * add order comments (database : wp_comment) if change payment status at backend
			 *
			 * @param int   $order_id order id.
			 * @param array $status_name status name.
			 * @return void
			 */
			private function zn_add_order_comments( $order_id, $status_name ) {

				$user = get_user_by( 'id', get_current_user_id() );

				$comments['order_id'] = $order_id;
				$comments['author'] = $user->display_name;
				$comments['email'] = $user->user_email;
				$comments['content'] = 'Order status changed from ' . $status_name['original'] . ' to ' . $status_name['new'] . '.';

				Zionpayment_General_Models::zn_add_db_order_comments( $comments );

			}

			/**
			 * Get Merchants Info for Version Tracker
			 *
			 * @return array
			 */
			public function zn_get_merchant_info() {

				$merchant_info['transaction_mode'] = $this->settings['server_mode'];
				$merchant_info['ip_address'] = isset( $_SERVER['SERVER_ADDR'] ) ? sanitize_text_field( wp_unslash( $_SERVER['SERVER_ADDR'] ) ) : ''; // input var okay.
				$merchant_info['shop_version'] = WC()->version;
				$merchant_info['plugin_version'] = constant( 'ZIONPAYMENT_VERSION' );
				$merchant_info['client'] = 'Zionpayment';
				$merchant_info['merchant_id'] = get_option( 'zionpayment_general_merchant_no' );
				$merchant_info['shop_system'] = 'WOOCOMMERCE';
				$merchant_info['email'] = get_option( 'zionpayment_general_merchant_email' );
				$merchant_info['shop_url'] = get_option( 'zionpayment_general_shop_url' );

				return $merchant_info;
			}

		} // End of class Zionpayment_Payment_Gateway
	}// End if().

	/**
	 * Gets zionpayment payment method
	 *
	 * @access public
	 * @param array $methods methods.
	 * @return array
	 */
	function get_zionpayment_payments( $methods ) {
		$methods[] = 'Gateway_Zionpayment_CC';
		//$methods[] = 'Gateway_Zionpayment_CCSaved';

		return $methods;
	}

	add_filter( 'woocommerce_payment_gateways', 'get_zionpayment_payments' );
	foreach ( glob( dirname( __FILE__ ) . '/includes/gateways/*.php' ) as $filename ) {
		if(!empty($filename)) {
			include_once $filename;
		}
	}
}


add_action( 'wp_ajax_my_action', 'get_ajax_registered_payment' );
/**
 * Get Registered Payment (AJAX)
 * Handle ajax request from add payment methods at backend order
 * Echo json
 *
 * @return void
 */
function get_ajax_registered_payment() {
	global $wpdb;

	$user_id = Zionpayment_General_Functions::zn_get_request_value( 'user_id' );
	$payment_id = Zionpayment_General_Functions::zn_get_request_value( 'payment_id' );
	$payment_group = '';

	if ( 'zionpayment_ccsaved' === $payment_id ) {
		$payment_group = 'CC';
	}

	$registered_payment = $wpdb->get_results( $wpdb->prepare( "SELECT  * FROM {$wpdb->prefix}zionpayment_payment_recurring WHERE cust_id = %d AND payment_group = %s", $user_id, $payment_group ), ARRAY_A ); // db call ok; no-cache ok.
	echo wp_json_encode( $registered_payment );

	wp_die(); // this is required to terminate immediately and return a proper response.

}
