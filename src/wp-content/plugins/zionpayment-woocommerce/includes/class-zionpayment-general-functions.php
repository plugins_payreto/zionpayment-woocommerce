<?php
/**
 * Zionpayment General Functions
 *
 * General functions available on both the front-end and admin.
 * Copyright (c) Zionpayment
 *
 * @class       Zionpayment_General_Functions
 * @package     Zionpayment/Classes
 * @located at  /includes/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * General functions available on both the front-end and admin.
 */
class Zionpayment_General_Functions {

	/**
	 * Percent for discount or  tax
	 *
	 * @var int $percent percent.
	 */
	public static $percent = 100;

	/**
	 * Get the plugin url.
	 *
	 * @return string
	 */
	public static function zn_get_plugin_url() {

		return untrailingslashit( plugins_url( '/', ZIONPAYMENT_PLUGIN_FILE ) );
	}

	/**
	 * Return the Wordpress language
	 *
	 * @return string
	 */
	public static function zn_get_shop_language() {

	    return ( 'de' === substr( get_bloginfo( 'language' ), 0, 2 ) ) ? 'de' : 'en' ;
	}

	/**
	 * Get Customer IP.
	 *
	 * @return string
	 */
	public static function get_customer_ip() {

		$server_remote_addr = '';
		$server = $_SERVER; // input var okay.
		if ( isset( $server['REMOTE_ADDR'] ) ) {
			$server_remote_addr = sanitize_text_field( wp_unslash( $server['REMOTE_ADDR'] ) );
		}

		if ( '::1' === $server_remote_addr ) {
			$customer_ip = '127.0.0.1';
		} else {
			$customer_ip = $server_remote_addr;
		}

		return $customer_ip;
	}

	/**
	 * Get Payment Variable.
	 *
	 * @param string $payment_id payment id.
	 * @return array
	 */
	public static function get_payment_gateway( $payment_id ) {

		$payment_gateway = array();
		$payment_gateway['payment_type'] = self::zn_get_payment_type( $payment_id );
		$payment_gateway['payment_brand'] = self::zn_get_payment_brand( $payment_id );
		$payment_gateway['payment_group'] = self::zn_get_payment_group( $payment_id );
		$payment_gateway['is_recurring'] = self::is_recurring( $payment_id );
		$payment_gateway['language'] = self::zn_get_shop_language();

		return $payment_gateway;
	}

	/**
	 * Get Credentials from Database
	 *
	 * @param string $payment_id payment id.
	 * @param bool   $is_testmode_available is testmode available.
	 * @param bool   $is_multichannel_available is testmode available.
	 * @return array
	 */
	public static function zn_get_credentials( $payment_id, $is_testmode_available = true, $is_multichannel_available = false ) {

		$payment_settings = get_option( 'woocommerce_' . $payment_id . '_settings' );

		$credentials = array();
		$credentials['login']       = get_option( 'zionpayment_general_login' );
		$credentials['password']    = get_option( 'zionpayment_general_password' );
		$credentials['server_mode'] = $payment_settings['server_mode'];

		if ( ! empty( $payment_settings['multichannel'] ) && $is_multichannel_available ) {
			$credentials['channel_id']  = $payment_settings['channel_moto'];
		} else {
			$credentials['channel_id']  = $payment_settings['channel_id'];
		}

		if ( $is_testmode_available ) {
			$credentials['test_mode'] = self::zn_get_test_mode( $payment_id, $credentials['server_mode'] );
		}

		return $credentials;
	}

	/**
	 * Get Test Mode
	 *
	 * @param string $payment_id payment id.
	 * @param string $server_mode server mode.
	 * @return boolean ( EXTERNAL/INTERNAL )
	 */
	public static function zn_get_test_mode( $payment_id, $server_mode ) {

		if ( 'LIVE' === $server_mode ) {
			return false;
		} else {
			return 'EXTERNAL';
		}
	}

	/**
	 * Get Recurring Payment
	 *
	 * @return array
	 */
	public static function zn_get_recurring_payment() {
		$payments = array( 'zionpayment_ccsaved' );

		return $payments;
	}

	/**
	 * Get Payment Brand
	 *
	 * @param string $payment_id payment id.
	 * @return string
	 */
	public static function zn_get_payment_brand( $payment_id ) {

		switch ( $payment_id ) {
			case 'zionpayment_cc':
			case 'zionpayment_ccsaved':
				$payment_settings = get_option( 'woocommerce_' . $payment_id . '_settings' );
				$cards = $payment_settings['card_types'];
				$payment_brand = '';
				if ( isset( $cards ) && '' !== $cards ) {
	                foreach ( $cards as $card ) {
	                    $payment_brand .= strtoupper( $card ) . ' ';
	                }
				}
				break;
			default:
				$payment_brand = false;
				break;
		}

		return $payment_brand;
	}

	/**
	 * Get Payment Form
	 *
	 * @param string $payment_id payment id.
	 * @return string
	 */
	public static function zn_get_payment_form( $payment_id ) {
		return 'form';
	}

	/**
	 * Get Payment Type
	 *
	 * @param string $payment_id payment id.
	 * @return string
	 */
	public static function zn_get_payment_type( $payment_id ) {

		switch ( $payment_id ) {
			case 'zionpayment_cc':
			case 'zionpayment_ccsaved':
				$payment_settings = get_option( 'woocommerce_' . $payment_id . '_settings' );
				$payment_type = $payment_settings['trans_mode'];
				break;
			default:
				$payment_type = 'DB';
		}

		return $payment_type;
	}

	/**
	 * Get Payment Group
	 *
	 * @param string $payment_id payment id.
	 * @return string
	 */
	public static function zn_get_payment_group( $payment_id ) {

		switch ( $payment_id ) {
			case 'zionpayment_ccsaved':
				$payment_group = 'CC';
				break;
			default:
				$payment_group = false;
		}

		return $payment_group;
	}

	/**
	 * Get Payment Id
	 *
	 * @param string $payment_group payment group.
	 * @return string
	 */
	public static function zn_get_payment_id_by_group( $payment_group ) {

		switch ( $payment_group ) {
			case 'CC':
				$payment_id = 'zionpayment_ccsaved';
				break;
			default:
				$payment_id = false;
		}

		return $payment_id;
	}

	/**
	 * Is recuring
	 *
	 * @param string $payment_id payment id.
	 * @return bool
	 */
	public static function is_recurring( $payment_id ) {
		switch ( $payment_id ) {
			case 'zionpayment_ccsaved':
				return true;
			default:
				return false;
		}
	}

	/**
	 * Get Account Card Info from gateway
	 *
	 * @param string $payment_id payment id.
	 * @param array  $payment_response payment response.
	 * @return array
	 */
	public static function zn_get_account_by_payment_response( $payment_id, $payment_response ) {

		if ( 'zionpayment_ccsaved' === $payment_id ) {
			$account_card['holder'] = $payment_response['card']['holder'];
			$account_card['last_4_digits'] = $payment_response['card']['last4Digits'];
			$account_card['expiry_month'] = $payment_response['card']['expiryMonth'];
			$account_card['expiry_year'] = $payment_response['card']['expiryYear'];
			$account_card['email'] = '';
		}

		return $account_card;
	}

	/**
	 * Get Payment Price with Tax and Discount.
	 *
	 * @param array $cart cart.
	 * @return float
	 */
	public static function zn_get_payment_price_with_tax_and_discount( $cart ) {

		$is_prices_include_tax = get_option( 'woocommerce_prices_include_tax' );

		$price_with_discount = $cart['data']->price;
		if ( 'no' === $is_prices_include_tax ) {
			$tax = $cart['line_tax'] / $cart['quantity'];
			$price_with_discount_and_tax = $cart['data']->price + $tax;
		} else {
			$price_with_discount_and_tax = $cart['data']->price;
		}

		return Zionpayment_Payment_Core::set_number_format( $price_with_discount_and_tax );
	}

	/**
	 * Get Payment Discount from Cart Order.
	 *
	 * @param array $cart cart.
	 * @return float
	 */
	public static function zn_get_payment_discount_in_percent( $cart ) {

		$product_detail = Zionpayment_General_Models::zn_get_db_product_detail( $cart['data']->id );

		$regular_price = $product_detail['_regular_price'];
		$sale_price = $cart['data']->price;

		if ( $regular_price !== $sale_price ) {
			$discount = $regular_price - $sale_price;
			$discount_percent = ($discount / $regular_price) * self::$percent;

			return Zionpayment_Payment_Core::set_number_format( $discount_percent );
		}

		return false;
	}

	/**
	 * Get Payment Tax from Cart Order.
	 *
	 * @param array $cart cart.
	 * @return float
	 */
	public static function zn_get_payment_tax_in_percent( $cart ) {

		$is_enable_tax = get_option( 'woocommerce_calc_taxes' );
		$product_detail = Zionpayment_General_Models::zn_get_db_product_detail( $cart['data']->id );
		$is_enable_tax_product = $product_detail['_tax_status'];

		if ( 'yes' === $is_enable_tax && 'taxable' === $is_enable_tax_product ) {

			$tax_precent = ($cart['line_tax'] / $cart['line_total']) * self::$percent;
			return Zionpayment_Payment_Core::set_number_format( $tax_precent );
		}

		return false;
	}


	/**
	 * Translate Frontend Payment Name.
	 *
	 * @param string $payment_id payment id.
	 * @return string
	 */
	public static function zn_translate_frontend_payment( $payment_id ) {

		switch ( $payment_id ) {
			case 'zionpayment_cc':
				$payment_locale = __( 'FRONTEND_PM_CC', 'wc-zionpayment' );
				break;
			case 'zionpayment_ccsaved':
				$payment_locale = __( 'FRONTEND_PM_CCSAVED', 'wc-zionpayment' );
				break;
		}

		return $payment_locale;
	}

	/**
	 * Translate Backend Payment Name.
	 *
	 * @param string $payment_id payment id.
	 * @return string
	 */
	public static function zn_translate_backend_payment( $payment_id ) {

		switch ( $payment_id ) {
			case 'zionpayment_cc':
				$payment_locale = __( 'BACKEND_PM_CC', 'wc-zionpayment' );
				break;
			case 'zionpayment_ccsaved':
				$payment_locale = __( 'BACKEND_PM_CCSAVED', 'wc-zionpayment' );
				break;
		}

		return $payment_locale;
	}

	/**
	 * Translate Error Identifier.
	 *
	 * @param string $error_identifier error identifier.
	 * @return string
	 */
	public static function zn_translate_error_identifier( $error_identifier ) {

		switch ( $error_identifier ) {

			case 'ERROR_CC_ACCOUNT':
				$error_translate = __( 'ERROR_CC_ACCOUNT', 'wc-zionpayment' );
				break;
			case 'ERROR_CC_INVALIDDATA':
				$error_translate = __( 'ERROR_CC_INVALIDDATA', 'wc-zionpayment' );
				break;
			case 'ERROR_CC_BLACKLIST':
				$error_translate = __( 'ERROR_CC_BLACKLIST', 'wc-zionpayment' );
				break;
			case 'ERROR_CC_DECLINED_CARD':
				$error_translate = __( 'ERROR_CC_DECLINED_CARD', 'wc-zionpayment' );
				break;
			case 'ERROR_CC_EXPIRED':
				$error_translate = __( 'ERROR_CC_EXPIRED', 'wc-zionpayment' );
				break;
			case 'ERROR_CC_INVALIDCVV':
				$error_translate = __( 'ERROR_CC_INVALIDCVV', 'wc-zionpayment' );
				break;
			case 'ERROR_CC_EXPIRY':
				$error_translate = __( 'ERROR_CC_EXPIRY', 'wc-zionpayment' );
				break;
			case 'ERROR_CC_LIMIT_EXCEED':
				$error_translate = __( 'ERROR_CC_LIMIT_EXCEED', 'wc-zionpayment' );
				break;
			case 'ERROR_CC_3DAUTH':
				$error_translate = __( 'ERROR_CC_3DAUTH', 'wc-zionpayment' );
				break;
			case 'ERROR_CC_3DERROR':
				$error_translate = __( 'ERROR_CC_3DERROR', 'wc-zionpayment' );
				break;
			case 'ERROR_CC_NOBRAND':
				$error_translate = __( 'ERROR_CC_NOBRAND', 'wc-zionpayment' );
				break;
			case 'ERROR_GENERAL_LIMIT_AMOUNT':
				$error_translate = __( 'ERROR_GENERAL_LIMIT_AMOUNT', 'wc-zionpayment' );
				break;
			case 'ERROR_GENERAL_LIMIT_TRANSACTIONS':
				$error_translate = __( 'ERROR_GENERAL_LIMIT_TRANSACTIONS', 'wc-zionpayment' );
				break;
			case 'ERROR_CC_DECLINED_AUTH':
				$error_translate = __( 'ERROR_CC_DECLINED_AUTH', 'wc-zionpayment' );
				break;
			case 'ERROR_GENERAL_DECLINED_RISK':
				$error_translate = __( 'ERROR_GENERAL_DECLINED_RISK', 'wc-zionpayment' );
				break;
			case 'ERROR_CC_ADDRESS':
				$error_translate = __( 'ERROR_CC_ADDRESS', 'wc-zionpayment' );
				break;
			case 'ERROR_GENERAL_CANCEL':
				$error_translate = __( 'ERROR_GENERAL_CANCEL', 'wc-zionpayment' );
				break;
			case 'ERROR_CC_RECURRING':
				$error_translate = __( 'ERROR_CC_RECURRING', 'wc-zionpayment' );
				break;
			case 'ERROR_CC_REPEATED':
				$error_translate = __( 'ERROR_CC_REPEATED', 'wc-zionpayment' );
				break;
			case 'ERROR_GENERAL_ADDRESS':
				$error_translate = __( 'ERROR_GENERAL_ADDRESS', 'wc-zionpayment' );
				break;
			case 'ERROR_GENERAL_BLACKLIST':
				$error_translate = __( 'ERROR_GENERAL_BLACKLIST', 'wc-zionpayment' );
				break;
			case 'ERROR_GENERAL_GENERAL':
				$error_translate = __( 'ERROR_GENERAL_GENERAL', 'wc-zionpayment' );
				break;
			case 'ERROR_GENERAL_TIMEOUT':
				$error_translate = __( 'ERROR_GENERAL_TIMEOUT', 'wc-zionpayment' );
				break;
			case 'ERROR_CAPTURE_BACKEND':
				$error_translate = __( 'ERROR_CAPTURE_BACKEND', 'wc-zionpayment' );
				break;
			case 'ERROR_REORDER_BACKEND':
				$error_translate = __( 'ERROR_REORDER_BACKEND', 'wc-zionpayment' );
				break;
			case 'ERROR_REFUND_BACKEND':
				$error_translate = __( 'ERROR_REFUND_BACKEND', 'wc-zionpayment' );
				break;
			case 'ERROR_RECEIPT_BACKEND':
				$error_translate = __( 'ERROR_RECEIPT_BACKEND', 'wc-zionpayment' );
				break;
			case 'ERROR_ADDRESS_PHONE':
				$error_translate = __( 'ERROR_ADDRESS_PHONE', 'wc-zionpayment' );
				break;
			case 'ERROR_CAPTURE_BACKEND':
				$error_translate = __( 'ERROR_CAPTURE_BACKEND', 'wc-zionpayment' );
				break;
			case 'ERROR_REORDER_BACKEND':
				$error_translate = __( 'ERROR_REORDER_BACKEND', 'wc-zionpayment' );
				break;
			case 'ERROR_REFUND_BACKEND':
				$error_translate = __( 'ERROR_REFUND_BACKEND', 'wc-zionpayment' );
				break;
			case 'ERROR_RECEIPT_BACKEND':
				$error_translate = __( 'ERROR_RECEIPT_BACKEND', 'wc-zionpayment' );
				break;
			case 'ERROR_GENERAL_NORESPONSE':
				$error_translate = __( 'ERROR_GENERAL_NORESPONSE', 'wc-zionpayment' );
				break;
			case 'ERROR_GENERAL_REDIRECT':
				$error_translate = __( 'ERROR_GENERAL_REDIRECT', 'wc-zionpayment' );
				break;
			default:
				$error_translate = __( 'ERROR_UNKNOWN', 'wc-zionpayment' );
				break;
		}// End switch().

		return $error_translate;
	}

	/**
	 * Get _REQUEST value
	 *
	 * @param string $key key.
	 * @param boolean $default default.
	 * @return boolean | string
	 */
	public static function zn_get_request_value( $key, $default = false ) {
		if ( isset( $_REQUEST[ $key ] ) ) {// input var okay.
			return sanitize_text_field( wp_unslash( $_REQUEST[ $key ] ) ); // input var okay.
		}
		return $default;
	}

}
