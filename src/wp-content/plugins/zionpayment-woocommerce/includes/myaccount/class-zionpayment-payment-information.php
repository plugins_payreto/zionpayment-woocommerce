<?php
/**
 * Zionpayment Plugin Installation Process
 *
 * This class is used for My Payment Information
 * Copyright (c) Zionpayment
 *
 * @class       Zionpayment_Payment_Information
 * @package     Zionpayment/Classes
 * @located at  /includes/myaccount/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * The class is used for My Payment Information
 */
class Zionpayment_Payment_Information extends WC_Shortcode_My_Account {

	/**
	 * Recurring
	 *
	 * @var boolean $recurring
	 */
	static $recurring;
	/**
	 * Home url
	 *
	 * @var string $home_url
	 */
	static $home_url;
	/**
	 * Current url
	 *
	 * @var string $current_url
	 */
	static $current_url;
	/**
	 * Clean current url
	 *
	 * @var string $clean_current_url
	 */
	static $clean_current_url;
	/**
	 * Plugin url
	 *
	 * @var string $plugin_url
	 */
	static $plugin_url;
	/**
	 * Plugin path
	 *
	 * @var string $plugin_path
	 */
	static $plugin_path;

	/**
	 * Main function of the class
	 */
	public static function init() {

		global $wp;

		self::$home_url = home_url();
		if ( isset( $wp->request ) ) {
			self::$current_url = home_url( $wp->request ) . '/?';
			;
		} else {
			self::$current_url = get_page_link() . '&';
		}
		self::$clean_current_url = rtrim( self::$current_url, '?' );
		self::$plugin_url = plugins_url( '/', ZIONPAYMENT_PLUGIN_FILE );
		self::$plugin_path = dirname( __FILE__ ) . '/../..';

		if ( ! is_user_logged_in() ) {

			wp_safe_redirect( self::$home_url . '/my-account/' );
		} else {

			//self::$recurring = get_option( 'zionpayment_general_recurring' );
			self::$recurring = false;
			if ( self::$recurring ) {
				$section = Zionpayment_General_Functions::zn_get_request_value( 'section' );
				if ( !empty( $wp->query_vars['page'] ) ) {
					switch ( $wp->query_vars['page'] ) {
						case 'wc-register':
							self::zn_register_payment( $section );
							break;
						case 'wc-reregister':
							self::zn_change_payment( $section );
							break;
						case 'wc-deregister':
							self::zn_delete_payment( $section );
							break;
						case 'wc-response-register':
							self::zn_response_register( $section, 'register' );
							break;
						case 'wc-response-reregister':
							self::zn_response_register( $section, 'reregister' );
							break;
						case 'wc-response-deregister':
							self::zn_response_deregister( $section );
							break;
						case 'wc-default':
							self::zn_set_default_payment( $section );
							break;
					}
				} else {
					self::zn_get_payment_information_page();
				}
			} else {

				wp_safe_redirect( self::$home_url . '/my-account/' );
			}
		}// End if().

	}

	/**
	 * Payment Information Page
	 */
	private static function zn_get_payment_information_page() {

		$payment_recurring = Zionpayment_General_Functions::zn_get_recurring_payment();

		foreach ( $payment_recurring as $payment_id ) {
			$payment_group = Zionpayment_General_Functions::zn_get_payment_group( $payment_id );
			$credentials = Zionpayment_General_Functions::zn_get_credentials( $payment_id );
			$registered_payments[ $payment_group ] = Zionpayment_General_Models::zn_get_db_registered_payment( $payment_group, $credentials );

			$active_variable_name = 'is_active_' . strtolower( Zionpayment_General_Functions::zn_get_payment_group( $payment_id ) );
			$$active_variable_name = self::zn_is_active_payment( $payment_id );
		}

		wp_enqueue_style( 'zionpayment_formpayment_style', self::$plugin_url . '/assets/css/formpayment.css', array(), null );
		include( self::$plugin_path . '/templates/myaccount/template-payment-information.php' );
	}

	/**
	 * Check Payment Activation
	 *
	 * @param string $payment_id payment id.
	 * @return bool
	 */
	private static function zn_is_active_payment( $payment_id ) {

		$payment_settings = get_option( 'woocommerce_' . $payment_id . '_settings' );

		if ( 'yes' === $payment_settings['enabled'] ) {
			return true;
		} else {
			return false;
		}
	}


	/**
	 * Set Default Payment Information
	 *
	 * @param string $payment_id payment id.
	 */
	private static function zn_set_default_payment( $payment_id ) {

		$recurring_id = Zionpayment_General_Functions::zn_get_request_value( 'id' );

		if ( $recurring_id ) {

			$payment_group = Zionpayment_General_Functions::zn_get_payment_group( $payment_id );
			$credentials = Zionpayment_General_Functions::zn_get_credentials( $payment_id );

			$query['field'] = 'payment_default';
			$query['value'] = '1';
			$query['payment_default'] = 0;
			Zionpayment_General_Models::zn_update_db_default_payment( $query, $payment_group, $credentials );

			$query['field'] = 'id';
			$query['value'] = $recurring_id;
			$query['payment_default'] = 1;
			Zionpayment_General_Models::zn_update_db_default_payment( $query, $payment_group, $credentials );
		}

		wp_safe_redirect( self::$clean_current_url );

	}

	/**
	 * Register Payment Page
	 * Get Chekout Id & Payment Widget from Opp
	 *
	 * @param string $payment_id payment id.
	 */
	private static function zn_register_payment( $payment_id ) {

		$register_parameters = self::zn_get_register_parameters( $payment_id, true );
		$register_parameters['transaction_id'] = get_current_user_id();

		$payment_brand = Zionpayment_General_Functions::zn_get_payment_brand( $payment_id );
		$checkout_result = Zionpayment_Payment_Core::get_checkout_result($register_parameters);
		$url_config['payment_widget'] = Zionpayment_Payment_Core::get_payment_widget_url( $register_parameters, $checkout_result['id'] );
		$url_config['return_url'] = self::$current_url . 'page=wc-response-register&section=' . $payment_id;
		$url_config['cancel_url'] = self::$clean_current_url;

		$form_title = __( 'FRONTEND_MC_SAVE', 'wc-zionpayment' );
		$confirm_button = __( 'FRONTEND_BT_REGISTER', 'wc-zionpayment' );

		if ( ! $checkout_id ) {
			WC()->session->set( 'zionpayment_myaccount_error', __( 'ERROR_GENERAL_REDIRECT', 'wc-zionpayment' ) );
			wp_safe_redirect( self::$clean_current_url );
		}

		wp_enqueue_script( 'zionpayment_formpayment_script', $url_config['payment_widget'], array(), null );
		wp_enqueue_style( 'zionpayment_formpayment_style', self::$plugin_url . '/assets/css/formpayment.css', array(), null );
		include( self::$plugin_path . '/templates/myaccount/template-register.php' );

	}

	/**
	 * Change (ReRegisster) Payment Page
	 * Get Chekout Id & Payment Widget from Opp
	 *
	 * @param string $payment_id payment id.
	 */
	private static function zn_change_payment( $payment_id ) {

		$id = (int) Zionpayment_General_Functions::zn_get_request_value( 'id' );
		$registration_id = Zionpayment_General_Models::zn_get_db_registration_id( $id );
		$register_parameters = self::zn_get_register_parameters( $payment_id, true );
		$register_parameters['transaction_id'] = $registration_id;

		$payment_brand = Zionpayment_General_Functions::zn_get_payment_brand( $payment_id );
		$checkout_result = Zionpayment_Payment_Core::get_checkout_result($register_parameters);
		$url_config['payment_widget'] = Zionpayment_Payment_Core::get_payment_widget_url( $register_parameters, $checkout_id['id'] );
		$url_config['return_url'] = self::$current_url . 'page=wc-response-reregister&section=' . $payment_id . '&recurring_id=' . $id;
		$url_config['cancel_url'] = self::$clean_current_url;

		$form_title = __( 'FRONTEND_MC_CHANGE', 'wc-zionpayment' );
		$confirm_button = __( 'FRONTEND_BT_CHANGE', 'wc-zionpayment' );

		if ( ! $checkout_id ) {
			WC()->session->set( 'zionpayment_myaccount_error', __( 'ERROR_GENERAL_REDIRECT', 'wc-zionpayment' ) );
			wp_safe_redirect( self::$clean_current_url );
		}

		wp_enqueue_script( 'zionpayment_formpayment_script', $url_config['payment_widget'], array(), null );
		wp_enqueue_style( 'zionpayment_formpayment_style', self::$plugin_url . '/assets/css/formpayment.css', array(), null );
		include( self::$plugin_path . '/templates/myaccount/template-register.php' );

	}

	/**
	 * Delete (DeRegisster) Payment Page
	 *
	 * @param string $payment_id payment id.
	 */
	private static function zn_delete_payment( $payment_id ) {

		$id = Zionpayment_General_Functions::zn_get_request_value( 'id' );
		$url_config['return_url'] = self::$current_url . 'page=wc-response-deregister&section=' . $payment_id . '&recurring_id=' . $id;
		$url_config['cancel_url'] = self::$clean_current_url;

		wp_enqueue_style( 'zionpayment_formpayment_style', self::$plugin_url . '/assets/css/formpayment.css', array(), null );
		include( self::$plugin_path . '/templates/myaccount/template-deregister.php' );
	}

	/**
	 * Response from Register/ReRegister Payment
	 * Get Status & Response from Opp
	 *
	 * @param string $payment_id payment id.
	 * @param string $register_method register method.
	 */
	private static function zn_response_register( $payment_id, $register_method ) {

		$checkout_id = Zionpayment_General_Functions::zn_get_request_value( 'id' );
		$recurring_id =(int) Zionpayment_General_Functions::zn_get_request_value( 'recurring_id' );

		$is_testmode_available = false;
		$credentials = Zionpayment_General_Functions::zn_get_credentials( $payment_id, $is_testmode_available );
		$payment_response = Zionpayment_Payment_Core::get_payment_response( $checkout_id, $credentials );
		$payment_status = Zionpayment_Payment_Core::get_payment_status_by_result_code( $payment_response['result']['code'] );

		if ( $payment_response ) {

			if ( 'ACK' === $payment_status ) {

				self::zn_do_success_register( $payment_id, $payment_response, $recurring_id );
				if ( 'register' === $register_method ) {
					$success_message = __( 'SUCCESS_MC_ADD', 'wc-zionpayment' );
				} else {
					$success_message = __( 'SUCCESS_MC_UPDATE', 'wc-zionpayment' );
				}

				WC()->session->set( 'zionpayment_myaccount_success', $success_message );

			} else {
				if ( 'register' === $register_method ) {
					$error_message = __( 'ERROR_MC_ADD', 'wc-zionpayment' );
				} else {
					$error_message = __( 'ERROR_MC_UPDATE', 'wc-zionpayment' );
				}

				WC()->session->set( 'zionpayment_myaccount_error', $error_message );
			}
		} else {
			$error_message = __( 'ERROR_GENERAL_NORESPONSE', 'wc-zionpayment' );
			WC()->session->set( 'zionpayment_myaccount_error', $error_message );
		}
		wp_safe_redirect( self::$clean_current_url );
	}

	/**
	 * Do Success Register/ReRegister
	 * Action if register is success
	 *
	 * @param  string $payment_id        The payment identifier.
	 * @param  array  $payment_response  The payment response.
	 * @param  int    $recurring_id      The recurring identifier.
	 */
	private static function zn_do_success_register( $payment_id, $payment_response, $recurring_id ) {

		$deregister_id = $payment_response['merchantTransactionId'];
		self::zn_save_registered_payment( $payment_id, $payment_response, $recurring_id );

		$register_parameters = self::zn_get_register_parameters( $payment_id );
		if ( $recurring_id ) {
			$register_parameters['transaction_id'] = $payment_response['merchantTransactionId'];
		} else {
			$register_parameters['transaction_id'] = get_current_user_id();
		}

		$reference_id = $payment_response['id'];
		self::zn_refund_registered_payment( $reference_id, $register_parameters );

		if ( $recurring_id ) {
			Zionpayment_Payment_Core::delete_registered_account( $deregister_id, $register_parameters );
		}

	}

	/**
	 * Response from DeRegister Payment
	 * DeRegister & Get Response from Opp
	 *
	 * @param string $payment_id payment id.
	 */
	private static function zn_response_deregister( $payment_id ) {

		$recurring_id = (int) Zionpayment_General_Functions::zn_get_request_value( 'recurring_id', 0 );
		$registration_id = Zionpayment_General_Models::zn_get_db_registration_id( $recurring_id );

		$register_parameters = Zionpayment_General_Functions::zn_get_credentials( $payment_id );
		$register_parameters['transaction_id'] = get_current_user_id();

		$deregister_response = Zionpayment_Payment_Core::delete_registered_account( $registration_id, $register_parameters );
		$payment_status = Zionpayment_Payment_Core::get_payment_status_by_result_code( $deregister_response['result']['code'] );

		if ( 'ACK' === $payment_status ) {

			Zionpayment_General_Models::zn_delete_db_registered_payment( $recurring_id );
			WC()->session->set( 'zionpayment_myaccount_success', __( 'SUCCESS_MC_DELETE', 'wc-zionpayment' ) );
		} else {

			WC()->session->set( 'zionpayment_myaccount_error', __( 'ERROR_MC_DELETE', 'wc-zionpayment' ) );
		}

		wp_safe_redirect( self::$clean_current_url );
	}

	/**
	 * Refund Registered Payment
	 * Refund & Capture/Reversal Amount from Registered Payment
	 *
	 * @param int   $reference_id reference id.
	 * @param array $register_parameters register parameters.
	 */
	private static function zn_refund_registered_payment( $reference_id, $register_parameters ) {

		$payment_type = $register_parameters['payment_type'];
		$register_parameters['payment_type'] = ( 'DB' === $payment_type )? 'RF' : 'CP';
		$backoffice_operation_response = Zionpayment_Payment_Core::backoffice_operation( $reference_id, $register_parameters );

		if ( 'PA' === $payment_type ) {

			$reference_id = $backoffice_operation_response['id'];
			$register_parameters['payment_type'] = 'RF';
			Zionpayment_Payment_Core::backoffice_operation( $reference_id, $register_parameters );
		}

	}

	/**
	 * Get Parameter for Registered Payment
	 *
	 * @param string $payment_id payment id.
	 * @param bool   $registration recuring or not.
	 * @return array
	 */
	private static function zn_get_register_parameters( $payment_id, $registration = false ) {

		$payment_settings = get_option( 'woocommerce_' . $payment_id . '_settings' );
		$register_parameters = Zionpayment_General_Functions::zn_get_credentials( $payment_id );

		$register_parameters['amount']         = $payment_settings['amount_registration'];
		$register_parameters['currency']       = get_woocommerce_currency();
		$register_parameters['payment_type']   = Zionpayment_General_Functions::zn_get_payment_type( $payment_id );

		if ( $registration ) {
			$register_parameters['payment_recurring'] = 'INITIAL';
			$register_parameters['payment_registration'] = 'true';

		}

		if ( 'zionpayment_ccsaved' === $payment_id ) {
			$register_parameters['3D']['amount']    = $register_parameters['amount'] ;
			$register_parameters['3D']['currency']  = $register_parameters['currency'];
		}

		$register_parameters['customer']['first_name']     = esc_attr( get_the_author_meta( 'billing_first_name', get_current_user_id() ) );
		$register_parameters['customer']['last_name']      = esc_attr( get_the_author_meta( 'billing_last_name', get_current_user_id() ) );
		$register_parameters['customer']['phone']          = esc_attr( get_the_author_meta( 'billing_phone', get_current_user_id() ) );
		$register_parameters['customer']['email']          = esc_attr( get_the_author_meta( 'billing_email', get_current_user_id() ) );
		$register_parameters['billing']['street']          = esc_attr( get_the_author_meta( 'billing_address_1', get_current_user_id() ) );
		$register_parameters['billing']['city']            = esc_attr( get_the_author_meta( 'billing_city', get_current_user_id() ) );
		$register_parameters['billing']['zip']             = esc_attr( get_the_author_meta( 'billing_postcode', get_current_user_id() ) );
		$register_parameters['billing']['country_code']    = esc_attr( get_the_author_meta( 'billing_country', get_current_user_id() ) );

		$register_parameters['customer_ip'] = Zionpayment_General_Functions::get_customer_ip();

		return $register_parameters;

	}

	/**
	 * Save Registered Payment to DB
	 *
	 * @param string $payment_id payment id.
	 * @param array  $payment_response payment response.
	 * @param int    $recurring_id recurring id.
	 */
	private static function zn_save_registered_payment( $payment_id, $payment_response, $recurring_id ) {

		global $wpdb;

		$payment_group = Zionpayment_General_Functions::zn_get_payment_group( $payment_id );
		$credentials = Zionpayment_General_Functions::zn_get_credentials( $payment_id );
		$registered_payment = Zionpayment_General_Functions::zn_get_account_by_payment_response( $payment_id, $payment_response );

		$registered_payment['payment_brand'] = $payment_response['paymentBrand'];
		$registered_payment['payment_group'] = Zionpayment_General_Functions::zn_get_payment_group( $payment_id );
		$registered_payment['payment_default'] = Zionpayment_General_Models::zn_get_db_payment_default( $payment_group, $credentials );
		$registered_payment['server_mode'] = $credentials['server_mode'];
		$registered_payment['channel_id'] = $credentials['channel_id'];
		$registered_payment['registration_id'] = $payment_response['registrationId'];

		if ( $recurring_id ) {
			Zionpayment_General_Models::zn_update_db_registered_payment( $recurring_id, $registered_payment );
		} else { Zionpayment_General_Models::zn_save_db_registered_payment( $registered_payment );
		}

	}

}

add_shortcode( 'woocommerce_my_payment_information', 'Zionpayment_Payment_Information::init' );


