<?php
/**
 * Zionpayment Credit Card Recurring
 *
 * This gateway is used for Credit card Recurring.
 * Copyright (c) Zionpayment
 *
 * @class       Gateway_Zionpayment_CCSaved
 * @package     Zionpayment/Gateway
 * @extends     Zionpayment_Payment_Gateway
 * @located at  /includes/gateways
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * The gateway is used for Credit card Recurring.
 */
class Gateway_Zionpayment_CCSaved extends Zionpayment_Payment_Gateway {

	/**
	 * Identifier CCSaved
	 *
	 * @var string $id
	 */
	public $id = 'zionpayment_ccsaved';

	/**
	 * Returns the gateway icon.
	 * WC_Payment_Gateway - function
	 */
	public function get_icon() {

		$icon_html = $this->zn_get_multi_icon();
		return apply_filters( 'woocommerce_gateway_icon', $icon_html, $this->id );
	}

}

$obj = new Gateway_Zionpayment_CCSaved();


