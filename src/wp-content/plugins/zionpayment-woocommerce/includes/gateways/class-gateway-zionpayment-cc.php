<?php
/**
 * Zionpayment Credit Card
 *
 * This gateway is used for Credit card.
 * Copyright (c) Zionpayment
 *
 * @class       Gateway_Zionpayment_CC
 * @package     Zionpayment/Gateway
 * @extends     Zionpayment_Payment_Gateway
 * @located at  /includes/gateways
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * The gateway is used for Credit card.
 */
class Gateway_Zionpayment_CC extends Zionpayment_Payment_Gateway {

	/**
	 * Identifier CC
	 *
	 * @var string $id
	 */
	public $id = 'zionpayment_cc';

	/**
	 * From class WC_Payment_Gateway
	 * Payment gateway icon.
	 */
	public function get_icon() {

		$icon_html = $this->zn_get_multi_icon();
		return apply_filters( 'woocommerce_gateway_icon', $icon_html, $this->id );
	}

}

$obj = new Gateway_Zionpayment_CC();


