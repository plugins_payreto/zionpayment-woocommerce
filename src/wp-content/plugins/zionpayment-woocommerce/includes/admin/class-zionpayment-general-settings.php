<?php
/**
 * Zionpayment Plugin Installation Process
 *
 * This class is used for General Setting Tabs
 * Copyright (c) Zionpayment
 *
 * @class       Zionpayment_General_Settings
 * @package     Zionpayment/Classes
 * @located at  /includes/admin/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * The class is used for General Setting Tabs
 */
class Zionpayment_General_Settings {

	/**
	 * Tab name
	 *
	 * @var string
	 */
	public static $tab_name         = 'zionpayment_settings';

	/**
	 * Mandatory field
	 *
	 * @var array
	 */
	public static $mandatory_field	= array(
		'zionpayment_general_merchant_email',
		'zionpayment_general_merchant_no',
		'zionpayment_general_shop_url',
	);

	/**
	 * Main function of the class
	 */
	public static function init() {

		$page_request = Zionpayment_General_Functions::zn_get_request_value( 'page' );
		$tab_request = Zionpayment_General_Functions::zn_get_request_value( 'tab' );
		if ( 'wc-settings' === $page_request && 'zionpayment_settings' === $tab_request ) { // safe request.
			self::zn_save_tab_settings();
		}

		add_filter( 'woocommerce_settings_tabs_array',array( __CLASS__, 'add_settings_tab' ), 50 );
		add_action( 'woocommerce_settings_tabs_zionpayment_settings', array( __CLASS__, 'add_settings_page' ) );
		add_action( 'woocommerce_update_options_zionpayment_settings', array( __CLASS__, 'update_settings' ) );
	}

	/**
	 * Adds Zionpayment Tab to WooCommerce
	 * Calls from the hook "woocommerce_settings_tazn_array"
	 *
	 * @param array $woocommerce_tab woocommerce tab.
	 * @return array
	 */
	public static function add_settings_tab( $woocommerce_tab ) {
		$woocommerce_tab[ self::$tab_name ] = 'Zionpayment ' . __( 'BACKEND_CH_GENERAL', 'wc-zionpayment' );
		return $woocommerce_tab;
	}

	/**
	 * Adds Settings Fields to The Individual Sections
	 * Calls from the hook "woocommerce_settings_tazn_" {tab_name}
	 */
	public static function add_settings_page() {
		woocommerce_admin_fields( self::zn_settings_fields() );
	}

	/**
	 * Updates Settings Fields from Individual Sections
	 * Calls from the hook "woocommerce_update_options_" {tab_name}
	 */
	public static function update_settings() {
		woocommerce_update_options( self::zn_settings_fields() );
	}

	/**
	 * Adds Setting Fields for Zionpayment Global Configuration
	 */
	public static function zn_settings_fields() {
		global $zionpayment_payments;

		$settings = apply_filters('woocommerce_' . self::$tab_name, array(
			array(
				'title' => 'Zionpayment ' . __( 'BACKEND_CH_GENERAL', 'wc-zionpayment' ),
				'id'    => 'zionpayment_general_settings',
				'desc'  => '',
				'type'  => 'title',
			),
			array(
				'title' => __( 'BACKEND_CH_LOGIN', 'wc-zionpayment' ),
				'id'    => 'zionpayment_general_login',
				'css'   => 'width:25em;',
				'type'  => 'text',
			),
			array(
				'title' => __( 'BACKEND_CH_PASSWORD', 'wc-zionpayment' ),
				'id'    => 'zionpayment_general_password',
				'css'   => 'width:25em;',
				'type'  => 'text',
			),
			// array(
			// 	'title' => __( 'BACKEND_CH_RECURRING', 'wc-zionpayment' ),
			// 	'id'    => 'zionpayment_general_recurring',
			// 	'css'   => 'width:25em;',
			// 	'type'  => 'select',
			// 	'options' => array(
			// 		'0' => __( 'BACKEND_BT_NO', 'wc-zionpayment' ),
			// 		'1' => __( 'BACKEND_BT_YES', 'wc-zionpayment' ),
			// 	),
			// 	'default' => '1',
			// ),
			array(
				'title' => __( 'BACKEND_GENERAL_MERCHANTEMAIL', 'wc-zionpayment' ) . ' *',
				'id'    => 'zionpayment_general_merchant_email',
				'css'   => 'width:25em;',
				'type'  => 'text',
			),
			array(
				'title' => __( 'ZIONPAYMENT_BACKEND_GENERAL_MERCHANTNO', 'wc-zionpayment' ) . ' *',
				'id'    => 'zionpayment_general_merchant_no',
				'css'   => 'width:25em;',
				'type'  => 'text',
				'desc'	=> '<br />' . __( 'BACKEND_TT_MERCHANT_ID', 'wc-zionpayment' ),
			),
			array(
				'title' => __( 'BACKEND_GENERAL_SHOPURL', 'wc-zionpayment' ) . ' *',
				'id'    => 'zionpayment_general_shop_url',
				'css'   => 'width:25em;',
				'type'  => 'text',
			),
			array(
				'type' => 'sectionend',
				'id' => 'zionpayment_vendor_script',
			),
		));
		return apply_filters( 'woocommerce_' . self::$tab_name, $settings );
	}

	/**
	 * Redirects Error form Save Tab Setting if Empty Mandatory Fields
	 */
	public static function zn_save_tab_settings() {

		$save_request = Zionpayment_General_Functions::zn_get_request_value( 'save' );
		if ( $save_request ) {
			$is_fill_mandatory_fields = self::zn_is_fill_mandatory_fields( $_REQUEST ); // input var okay.

			if ( ! $is_fill_mandatory_fields ) {
				$get = isset( $_GET ) ? $_GET : null; // input var okay.
				$redirect = get_admin_url() . 'admin.php?' . http_build_query( $get );
				$redirect = remove_query_arg( 'save' );
				$error_message = __( 'ERROR_GENERAL_MANDATORY', 'wc-zionpayment' );
				$redirect = add_query_arg( 'wc_error', rawurlencode( esc_attr( $error_message ) ), $redirect );
				wp_safe_redirect( $redirect );
				exit();
			}
		}
	}

	/**
	 * Validate Mandatory Fields from Global Settings
	 *
	 * @param array $request request.
	 * @return mixed
	 */
	public static function zn_is_fill_mandatory_fields( $request ) {

		foreach ( $request as $fields_name => $field_value ) {
			if ( in_array( $fields_name, self::$mandatory_field, true ) ) {
				if ( trim( $field_value ) === '' ) {
					return false;
				}
			}
		}

		return true;
	}
}

Zionpayment_General_Settings::init();
