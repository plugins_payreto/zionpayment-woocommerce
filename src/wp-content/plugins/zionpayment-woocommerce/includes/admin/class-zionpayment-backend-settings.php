<?php
/**
 * Zionpayment Plugin Installation Process
 *
 * This class is used for Gateway Backend Setting
 * Copyright (c) Zionpayment
 *
 * @class       Zionpayment_Backend_Settings
 * @package     Zionpayment/Classes
 * @located at  /includes/admin/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * The class is used for Gateway Backend Setting
 */
class Zionpayment_Backend_Settings {

	/**
	 * Payment configurations at backend
	 *
	 * @param int $payment_id payment id.
	 */
	public static function zn_create_backend_payment_settings( $payment_id ) {

	  	$form_fields = array(
			  'enabled'       => array(
				  'title'     => __( 'BACKEND_CH_ACTIVE', 'wc-zionpayment' ),
				  'type'      => 'checkbox',
				  'default'   => '',
			  ),
			  'server_mode'   => array(
				  'title'     => __( 'BACKEND_CH_SERVER', 'wc-zionpayment' ),
				  'type'      => 'select',
				  'options'   => array(
					  'TEST' => __( 'BACKEND_CH_MODE_TEST', 'wc-zionpayment' ),
					  'LIVE' => __( 'BACKEND_CH_MODE_LIVE', 'wc-zionpayment' ),
				  ),
				  'default'   => '0',
			  ),
		  );

	  	if ( 'zionpayment_cc' === $payment_id || 'zionpayment_ccsaved' === $payment_id ) {

	  		$form_fields['card_types'] = array(
	            'title'     => __( 'BACKEND_CH_MODE', 'wc-zionpayment' ),
	            'type'      => 'multiselect',
	            'options'   => array(
									'MASTER' => __( 'BACKEND_CC_MASTER', 'wc-zionpayment' ),
	            					'VISA' => __( 'BACKEND_CC_VISA', 'wc-zionpayment' ),
	            					'JCB' => __( 'BACKEND_CC_JCB', 'wc-zionpayment' ),
	            					'AMEX' => __( 'BACKEND_CC_AMEX', 'wc-zionpayment' ),
	            				),
			);
	  	}

	  	if ( 'zionpayment_cc' === $payment_id || 'zionpayment_ccsaved' === $payment_id ) {

	  		$form_fields['trans_mode'] = array(
	            'title'     => __( 'BACKEND_CH_MODE', 'wc-zionpayment' ),
	            'type'      => 'select',
	            'options'   => array(
					'DB' => __( 'BACKEND_CH_MODEDEBIT', 'wc-zionpayment' ),
					'PA' => __( 'BACKEND_CH_MODEPREAUTH', 'wc-zionpayment' ),
				),
	        );

	  	}

	  	if ( 'zionpayment_ccsaved' === $payment_id ) {

	  		$form_fields['amount_registration'] = array(
	            'title'     => __( 'BACKEND_CH_AMOUNT', 'wc-zionpayment' ),
	            'type'      => 'text',
	            'default'   => '',
	            'description'	=> __( 'BACKEND_TT_REGISTRATION_AMOUNT', 'wc-zionpayment' ),
	        );
	  	}

	  	if ( 'zionpayment_ccsaved' === $payment_id ) {

	  		$form_fields['multichannel'] = array(
	            'title'     => __( 'BACKEND_CH_MULTICHANNEL', 'wc-zionpayment' ),
	            'type'      => 'select',
	            'options'   => array(
					true => __( 'BACKEND_BT_YES', 'wc-zionpayment' ),
					false => __( 'BACKEND_BT_NO', 'wc-zionpayment' ),
				),
	            'default'	  => 0,
			  'description' => __( 'BACKEND_TT_MULTICHANNEL', 'wc-zionpayment' ),
	        );
	  	}

	  	$form_fields['channel_id'] = array(
		  'title'     => __( 'BACKEND_CH_CHANNEL', 'wc-zionpayment' ),
		  'type'      => 'text',
		  'default'   => '',
		);

		if ( 'zionpayment_ccsaved' === $payment_id ) {

			$form_fields['channel_moto'] = array(
	            'title'     => __( 'BACKEND_CH_MOTO', 'wc-zionpayment' ),
	            'type'      => 'text',
	            'default'   => '',
	            'description'	=> __( 'BACKEND_TT_CHANNEL_MOTO', 'wc-zionpayment' ),
	        );
		}

		return $form_fields;

	}

	/**
	 * Get payment title for backend setting
	 *
	 * @param int $payment_id payment id.
	 * @return string
	 */
	public static function zn_get_backend_payment_title( $payment_id ) {

		return 'Zionpayment ' . Zionpayment_General_Functions::zn_translate_backend_payment( $payment_id );

	}

}

