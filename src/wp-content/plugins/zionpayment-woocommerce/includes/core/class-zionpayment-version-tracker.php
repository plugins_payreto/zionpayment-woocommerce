<?php
/**
 * Zionpayment Version Tracker
 *
 * This class to send version tracker (every payments transaction)
 * Copyright (c) Zionpayment
 *
 * @class       Zionpayment_Version_Tracker
 * @package     Zionpayment/Classes
 * @located at  /includes/core/
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * The class to share IP, email address, etc with Zionpayment.
 */
class Zionpayment_Version_Tracker {
	/**
	 * Get the version tracker url
	 */
	private static function _get_version_tracker_url() {
		$_version_tracker_url = 'http://api.dbserver.payreto.eu/v1/tracker';
		return $_version_tracker_url;
	}

	/**
	 * Get response data from the API
	 *
	 * @param array  $data post data.
	 * @param string $url url.
	 * @return array
	 */
	private static function _get_response_data( $data, $url ) {
		$response = wp_remote_post( $url, array(
			'body' => $data,
			'sslverify' => false,
			)
		);
		if ( is_wp_error( $response ) ) {
			return false;
		}
		$response = wp_remote_retrieve_body( $response );
		return json_decode( $response, true );
	}

	/**
	 * Get version tracker parameter
	 *
	 * @param array $version_data version data.
	 * @return array
	 */
	private static function _get_version_tracker_parameter( $version_data ) {
		$data = array(
			'transaction_mode' => $version_data['transaction_mode'],
			'ip_address' => $version_data['ip_address'],
			'shop_version' => $version_data['shop_version'],
			'plugin_version' => $version_data['plugin_version'],
			'client' => $version_data['client'],
			'hash' => md5( $version_data['shop_version'] . $version_data['plugin_version'] . $version_data['client'] ),
		);

		if ( $version_data['shop_system'] ) {
			$data['shop_system'] = $version_data['shop_system'];
		}
		if ( $version_data['email'] ) {
			$data['email'] = $version_data['email'];
		}
		if ( $version_data['merchant_id'] ) {
			$data['merchant_id'] = $version_data['merchant_id'];
		}
		if ( $version_data['shop_url'] ) {
			$data['shop_url'] = $version_data['shop_url'];
		}

		return $data;
	}

	/**
	 * Send version tracker into the API
	 *
	 * @param array $version_data version data.
	 * @return array
	 */
	public static function send_version_tracker( $version_data ) {
		$post_data = self::_get_version_tracker_parameter( $version_data );
		$url = self::_get_version_tracker_url();
		return self::_get_response_data( $post_data, $url );
	}
}
